function NormInData (Fpass)
% Normalisation the frequencies from input data and transformation dB - Np
global DA As F0 Finf1 Finf2 dDA dAs N1 N2;
dBNp = 20/log(10);
DA = DA / dBNp;
NAs = size(As); 
for k = 1:NAs(2),
   As(1,k) = As(1,k)/Fpass; As(2,k) = As(2,k)/dBNp;
end;
for k = 1:length(F0), F0(k) = F0(k)/Fpass; end;
for k = 1:length(Finf1), Finf1(k) = Finf1(k)/Fpass; end;
for k = 1:length(Finf2), Finf2(k) = Finf2(k)/Fpass; end;
dDA = dDA / dBNp; dAs = dAs / dBNp;
