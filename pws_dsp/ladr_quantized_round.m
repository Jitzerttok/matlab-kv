function [bq,aq] = ladr_quantized_round(K,C,bits);

l_K=length(K);
l_C=length(C);

nfK=zeros(1,l_K);
nfC=zeros(1,l_C);

Kn=zeros(l_K);
Cn=zeros(l_C);

Kq=zeros(l_K);
Cq=zeros(l_C);


for i = 1:l_K
    [Kn(i),nfK(i)] = scaling(K(i));
    Kq(i) = a2dr(Kn(i),bits);
    Kq(i) = nfK(i)*Kq(i);
end

for i = 1:l_C
    [Cn(i),nfC(i)] = scaling(C(i));
    Cq(i) = a2dr(Cn(i),bits);
    Cq(i) = nfC(i)*Cq(i);
end


[bq,aq] = ladr2dir(Kq,Cq);

end