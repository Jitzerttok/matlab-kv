function [X, F] = NewtonRaphsonGlobal (X, eps, FUNC, JACOBIAN, CHANGEARG, varargin)
% function [X, F] = NewtonRaphsonGlobal (X, eps, FUNC, JACOBIAN, CHANGEARG, varagin)
%
% Solves a system of nonlinear equation fi(X) = 0, where i = 1,2,... n,  
% and X is n-dimensional vector, by globaly converged Newton-Raphson method. 
% Input data:
%   X - initial values for function arguments;
%   eps - accuracy in zeroing of the functions. Iterations stop when
%      all funcions are less or equal to eps (by absolute value).
%   FUNC - name of the m-file, containing the function, which 
%      calculates the functions fi. This function must be called
%      in the following manner: F = FUNC(X,par1,par2,...) where X is
%      the arguments and par1,par2, etc. are aditional parameters, 
%      necessary for calculations.
%   JACOBIAN - name of the m-file, containing the function, which
%      computes the Jacobian matrix [J] = [dfi/dxj], i,j =1,2,...n.
%      It must be caaled as follows: [J] = JACOBIAN(X,F,par1,par2...)
%      where J is the Jacobian (n x n matrix), X is the vector of 
%      function arguments, F - vector, containing the function values, 
%      par1, par2, etc. are additional parameters, necessary for 
%      calculations.
%   CHANGEARG - name of the m-file, containing the function, which 
%      changes the argument values. It is called as follows: 
%      [Xnew,A] = CHANGEARG(X,dX,par1,par2,..), where in X are 
%      the function arguments, dX are their changes, par1, par2,... 
%      are paremeters. Xnew are new arguments which are calculeted 
%      according to the formula Xnew = X + A * dX. A is value between 0
%      and 1, choosen according additional conitions (if necessary).
%   varargin are additional parameters that are necessary for 
%      all computations of functions, Jacobian and argument changing.
%      To all functions FUNC, JACOBIAN and CHANGEARG must be passed 
%      all parameters, listed in varargin.
% Output data:
%   X - the solution for function arguments
%   F - function values for the solution point.

while 1 == 1,
   F = feval (FUNC,X,varargin{:});
   k = 1;
   while k <= length(F),
      if abs(F(k)) > eps break; end;
      k = k + 1;
   end;
   if k > length(F) break; end;
   f0 = 0.5 * F' * F;
   DFDX = feval(JACOBIAN,X,F,varargin{:});
display(DFDX)
df = DFDX * F;
display(df)
   dX = - inv(DFDX) * F;
display(dX)
   [Xnew,alfa] = feval(CHANGEARG,X,dX,varargin{:});
   Fnew = feval (FUNC,Xnew,varargin{:});
   f1 = 0.5 * Fnew' * Fnew;
   if f1 >= f0 
      df0 = F' * DFDX * dX;
      a = df0 * alfa * alfa / (f0 + df0*alfa - f1);
      Xnew = X + a * dX;
   end;
   X = Xnew;
end;
