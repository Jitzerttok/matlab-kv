function pole_density(b,form)
n=2^b;
n1=2^(b+1);
q=1/n;
q1=1/n1;
p=0;
if strcmp(form,'direct')
	a1=-2;
	while a1<2
	   a2=-1;
   	while a2<1
      	c=[1 a1 a2];
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end;
      	end
      	a2=a2+q;
     	end
		a1=a1+q;
	end
elseif strcmp(form,'normal')
	a=-1;
	while a<1
   	b=0;
   	while b<1
      	c=[1 -2*a a^2+b^2];
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end;
      	end
      	b=b+q;
     	end
		a=a+q;
   end
elseif strcmp(form,'wave')
	y1=-1;
	while y1<1
   	y2=-1;
   	while y2<1
      	c=[1 y1-y2 1-y1-y2];
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end;
      	end
      	y2=y2+q;
     	end
		y1=y1+q;
	end
elseif strcmp(form,'eform')
	e1=0;
	while e1<2
   	e2=-1;
   	while e2<1
      	c=[1 (e1^2-1-e2) e2];	%pri smiana na znaka pred a1 filtura stava B4
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end
      	end
      	e2=e2+q;
     	end
		e1=e1+q;
	end
elseif strcmp(form,'avenhaus')
	c1=0;
	while c1<4
   	c2=0;
   	while c2<1
      	c=[1 -(2-c1) 1-c1+c1*c2];	%pri smiana na znaka pred a1 filtura stava B4
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end
      	end
      	c2=c2+q;
		end
		c1=c1+q;
	end
elseif strcmp(form,'kingsbury')
	c1=0;
	while c1<2
   	c2=0;
   	while c2<4
      	c=[1 -(2-c1*c2-c1^2) 1-c1*c2];	%pri smiana na znaka pred a1 filtura stava B4
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end
      	end
      	c2=c2+q;
		end
		c1=c1+q;
    end
elseif strcmp(form,'BQ3')
    c=-1;
    while c<1
   	d=0;
   	while d<1
      	cc=[1 -2+4*c+2*d-4*c*d 1-2*d];	%
      	p1=roots(cc);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end
      	end
      	d=d+q;
		end
		c=c+2*q;
end
elseif strcmp(form,'zolzer')
	c1=0;
	while c1<2
   	c2=0;
   	while c2<4
      	c=[1 -(2-c1*c2-c1^3) 1-c1*c2];	%pri smiana na znaka pred a1 filtura stava B4
      	p1=roots(c);
      	if abs(p1(1))<1
         	if imag(p1(1))~=0
            	p=[p;p1];
         	end
      	end
      	c2=c2+q;
		end
		c1=c1+q;
	end
end
   
zplane(-2,p);
axis([-1.1 1.1 0 1.1]);
grid;