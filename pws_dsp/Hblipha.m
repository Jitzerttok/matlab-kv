function [b1,b2,a,aA,dP] = Hblipha(nA,omP,appart);
%
% Design of halfband-filters with approximately linear phase
% --------------------------------------------------------------
%
% [b1,b2,a,aA,dP] = Hblipha(nA,om,appart) designs a pair of
% halfband-filters with approximately linear phase, to be imple-
% mented by coupling of the allpass of even degree nA, designed
% here with a delay-element of degree m = nA-1. The allpass will
% be designed either with maximally flat or with Chebyshev approxi-
% mation of the desired linear phase. Required are the subprograms
% flatdel.m or Chebylinph.m and locmax.m
%
% Lit.: Schuessler DSVII, Sect.3.8
%
% Input and output parameters:
% nA      : even degree of the allpass;
% omP     : desired normalized cutoff-frequency of the lowpass (< .5);
% appart  : 'flatg' groupdelay maximally flat at om = 0;
%         : 'Cheby' Chebyshev-Approximation of the linear phase;                  Phase fuer 0<=om<=omD;
%
% b1,b2;  : numerator polynomials of Lp- and Hp-transfer functions;
% a       : denominator polynomial of both transfer functions;
% aA      : denominator polynomial of the  allpass-transfer funktion.
% dP      : maximal deviation 1-abs(HLp(om)) for 0<=om<=omP;
%
% Lit.: H.W.Schuessler, P.Steffen: Recursive Halfband-Filters
%       AEU
% example 1: nA = 6; omP = .4; appart = 'flatg';
%            result: fig.6 a-d; fig.7 a-c;
% example 2: nA = 6; omP = .4; appart = 'Cheby';
%            result: fig.6 e-h; fig.7 d-f;

m = nA-1; n = nA/2; t0 = m/2; omc = 2*omP;      % Preparation
%
if strcmp(appart,'flatg')                       % Design  
   c = flatdel(n,t0);                           % of the
elseif strcmp(appart,'Cheby');                  % fullband-
   [c,d0] = Chebylinph(n,t0,omc);               % allpass
end;
L = length(c); aA = zeros(2,L); c = c(:)';      % Transfer to the
aA(1,:) = c; aA = aA(:)'; aA = aA(1:2*L-1);     % halfband-allpass

aA_ = fliplr(aA);                               % Calculation of
aA0 = [1 zeros(1,m)]; aA0_ = fliplr(aA0);       % the transfer
a = conv(aA,aA0);                               % functions of
b1 = .5*(conv(aA0_,aA) + conv(aA0,aA_));        % the halfband- 
b2 = .5*(conv(aA0_,aA) - conv(aA0,aA_));        % filters;

wP = exp(j*omP*pi);
dP = 1-abs(polyval(b1,wP)/polyval(a,wP));      % Calculation of dP;
