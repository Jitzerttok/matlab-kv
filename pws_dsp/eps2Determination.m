function [eps2,Fc,Ac] = eps2Determination (eps1, eps2)
% function [eps2,X,kX] = eps2Determination(eps1,eps2)
% Determination of eps2 - the ripples in the passband to be equal to DA.

% Definition of global variables
global DA As F0 Finf1 Finf2 dDA dAs N1 N2;

N0 = ones(length(F0));  
Ninf1 = ones(length(Finf1)); Ninf2 = ones(length(Finf2));
eps2u = eps2;
[Fc,Ac,OK] = Remez2(eps1,eps2u);
if OK == 0
   eps2 = 0 + i; return;
end;
k = 1;
while Fc(k) < 1 k = k+1; end;
Acmean = mean(Ac(1:k));
if abs(Acmean) < 0.5*dDA return; end;
if Acmean > 0
   while Acmean > 0
      eps2u = 2 * eps2u; Acmeanl = Acmean;
      [Fc,Ac,OK] = Remez2(eps1,eps2u);
      if OK == 0
         eps2 = 0 + i; return;
      end;
      k = 1;
      while Fc(k) < 1 k = k+1; end;
      Acmean = mean(Ac(1:k));
   end;
   eps2l = 0.5 * eps2u; Acmeanu = Acmean;
else
   eps2l = eps2u; 
   while Acmean < 0
      eps2l = 0.5 * eps2l; Acmeanu = Acmean;
      [Fc,Ac,OK] = Remez2(eps1,eps2l);
      if OK == 0
         eps2 = 0 + i; return;
      end;
      k = 1;
      while Fc(k) < 1 k = k+1; end;
      Acmean = mean(Ac(1:k));
   end;
   eps2u = 2 * eps2l; Acmeanl = Acmean;
end;
Acmean = 10 * dDA;
jj = 1;
while abs(Acmean) > 0.5*dDA 
   if jj == 0 eps2 = 0.5 * (eps2l + eps2u);
   else eps2 = (Acmeanu*eps2l - Acmeanl*eps2u) / (Acmeanu - Acmeanl); end;
   jj = 1 - jj;
   [Fc,Ac,OK] = Remez2(eps1,eps2);
   k = 1;
   while Fc(k) < 1 k = k+1; end;
   Acmean = mean(Ac(1:k));
   if Acmean < 0 eps2u = eps2; Acmeanu = Acmean;
   else eps2l = eps2; Acmeanl = Acmean; end;
end;
