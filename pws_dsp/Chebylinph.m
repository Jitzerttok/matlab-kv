function [c,Delta] = Chebylinph(n,t0,omc);
%
% Design of an allpass with Chebysh. approximation of a linear phase
% ------------------------------------------------------------------
%
% [c,Delta] = Chebylinph(n,t0,omc) designs an allpass of degree n,
% the phase ph(om)of which approximates the desired linear phase
% t0*om in the intervall [0 omc] in the Chebyshev sense.
%
% Input and output parameters:
% n       : degree of the allpass;
% t0      : desired delay;
% omc     : upper limit of the approximation interval;
% 
% c       : denominator polynomial of the transfer function of the
%           allpass;
% Delta   : resulting normalized phase deviation 
%           max(ph(om) -t0*om))/pi;
% Lit.: Lang,Markus.: Signal Processing,Vol.27,(1992),pp.87-98
%       Schuessler H.W.: DSVII Section 3.6
%
% Example: n = 6; t0 = 2.5; omc = .8;

% Preparation;
om = (0:1000)/1000; om = om(:); eps = 1e-10;
omi = (1:fix(omc*100))/100; omcind = round(omc*1000);  
omi = omi(:)*pi; bDi = omi*t0;
betai = .5*(n*omi + bDi);
%
A = sin(omi*[n-1:-1:0]-betai*ones(1,n));      % Starting solution
b = -sin(n*omi-betai);
c_ = A\b; c = [1;c_]; 

% Remez-Algorithmus
while 1
  c = c(:); n = length(c)-1; nue = n:-1:0;            % Calculating
  ba = unwrap(2*angle(exp(j*om*pi*nue)*c))/pi - n*om; % the extremal
  db = ba -t0*om;                                     % values;
  omax = locmax(db); omin = locmax(-db);
  omex = sort([omax;omin]); omex = [omex(2:n+1);omcind];
  d0 = median(abs(db(omex)));
  
  if max(abs(db(omex)))-d0 < eps; break;
  else
     omi = [om(omex(1:n));omc]*pi;
     i = 1:n+1; d = d0*pi; Delta = d0;
     
 while 2
     x = [c(2:n+1);d];
     betai = .5*((t0+n)*omi - d*(-1).^i');
     f = sin(omi*[n:-1:0]-betai*ones(1,n+1))*c;
     J = [sin(omi*[n-1:-1:0]-betai*ones(1,n)),...
         .5*diag((-1).^i)*cos(omi*[n:-1:0]-betai*ones(1,n+1))*c];
     dx = J\f; if abs(dx) < eps; break;
     else
     x = x -dx; c = [1; x(1:n)]; d = x(n+1);
 end; end; end; end;    
          