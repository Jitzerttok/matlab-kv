function [p,bn,z] = biliwz(wp,wb,wz);
%
% Bilnear transformation of the continuous w-domain to digital z-domain;
%-----------------------------------------------------------------------
% [p,bn,z] = biliwz(wp,bm,wz) transforms the poles-zeros description
% of a transfer function G(w) of a continuous system into the corresponding
% expression of the transfer function H(z) in the digital domain;
%
% Input and output parameters
% wp,bm,wz     : poles wp, konstant factor bm and zeros in the w-domain
% p,bn,n       : corresponding values in the z-domain;
%
wp = wp(:); n0 = length(wp);
p = [(1+wp)./(1-wp)]; bn0 = real(wb/prod(1-wp));
    if nargin ==2, n = (-1)*ones(n0,1); bn = bn0; end;
    if nargin ==3, wz = wz(:); m0 = length(wz);
z = [(1+wz)./(1-wz); (-1)*ones((n0-m0),1)];
bn = real(bn0*prod(1-wz));
end   