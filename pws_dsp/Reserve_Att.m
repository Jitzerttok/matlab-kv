function RA = Reserve_Att (F, eps1, eps2, Fp);
% function RA = Reserve_Att (F, N1, N2, F0, Finf1, Finf2, eps1, eps2, Fp, DA, As);
% Returns the reserve in the attenuation. In the passband this is the difference
% between DA and the filter attenuation, in the stopband - the difference between 
% filter attenuation and the specified minimal attenuation for this frequency.
% F - the frequency.
% N1, N2 - degrees of the both transfer functions.
% F0 contains the frequencies in the numerator of the char. function.
% Finf1 - attenuation poles of the first function
% Finf contains the frequencies in the denominator of the first char. function.
% eps is the ripple factor
% Fp - the upper limit of the passband.
% DA - maximal ripple in the passband (in Np).
% As - specifications in the stopband. In the first row are the frequencies, 
%      in the second - attenuations (in Np). To every part corresponds two 
%      columns - for the left bound and for the right bound.

global DA As F0 Finf1 Finf2 dDA dAs N1 N2;

Nz(1:length(F0)) = 1; Ninf1(1:length(Finf1)) = 1; Ninf2(1:length(Finf2)) = 1;
if (F < inf)
   K1 = ChFncVal(eps1,N1,F0,Nz,Finf1,Ninf1,F); 
   K2 = ChFncVal(eps2,N2,F0,Nz,Finf2,Ninf2,F); 
   A = (1 + K1*K1) * (1 + K2*K2); A = 0.5 * log(A);
   if F <= Fp RA = DA - A;
   else
      k = 2;
      while As(1,k) < F, 
         k = k + 2; end;
      if As(1,k) == inf AA = As(2,k);
      else 
         AA = As(2,k-1) + (F-As(1,k-1)) * (As(2,k)-As(2,k-1)) / (As(1,k)-As(1,k-1)); 
      end; 
      RA = A - AA;
   end;
else
   RA = inf;
end;
