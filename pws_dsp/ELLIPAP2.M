[K,Z,P] = ellipap2(n,Fp,Fs)
% [K,Z,P] = ellipap2(n,Fp,Fs)
%     Elliptic approximation of analog lowpass prototype
%     K - amplification of the filter
%     Z - contains the transfer function zeroes, normalised to passband edge
%     P - contains the transfer function poles
%     n - transfer function order
%     Fp - upper boundery of passband
%     Fs - lower boundery of stopband

M = Fs/Fp; KK = ellipke(M);
% Calculation the frequencies of zero attenuation
if rem(n,2) == 0
   I = 1;
   while I < n
      [FZ(I),C,D] = ellipj(I*KK/n,M); I = I + 2;
   end;
else
   I = 2;
   while I < n
      [FZ(I),C,D] = ellipj(I*KK/n,M); I = I + 2;
   end;
end;

   
   
