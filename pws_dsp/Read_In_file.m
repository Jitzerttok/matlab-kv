function [Fpass,DA,As,k1,k2,F0,Finf1,Finf2,dDA,dAs,Ok] = Read_In_File (InFile);
% function [Fpass,DA,As,k1,F0,Finf1,Finf2,dDA,dAs,Ok] = Read_In_File (InFile);
% Reads the input file for equiripple approximation of a LP filter 
% when received transfer function is in the form as a product of 
% two transfer function, each of which can be realized az a sum of
% two allpass functions.
% Fpass - the upper limit of the passband (in Hz).
% DA - maximal ripple in the passband (in dB).
% As - specifications in the stopband. In the first row are the frequencies, 
%      in the second - attenuations (in dB). To every part corresponds two 
%      columns - for the left bound and for the right bound
% k1, k2 - degree of s in the numerators of the both characteristic functions.
% F0 contains the frequencies in the numerator of the both char. functions.
% Finf1 contains the frequencies in the denominator of the first char. function.
% Finf2 contains the frequencies in the denominator of the second char. function.
% dDA - maximal inequality of the ripples in the passband
% dAs - maximal inequality of the minimas of the attenuation reserve in the stopband.
% Ok - flag. Ok = 1 if all is Ok and Ok = 0 if file is not found.
% 'InFile' is the name of the input text file, which contains the input data 
%    in the following order:
%    1) Passband edge (in Hz) and passband ripple (in dB) of the filter.
%    2) Number of the parts from which consists the stopband specifications
%    3) Description of the parts of the stopband specifications. Each part is 
%       described on one separate row in a following order:
%          left edge (in Hz), attenuation for the left edge (in dB),
%          right edge (in Hz), attenuation for the right edge (in dB).
%          For the infinite frequency (in the last part of specifications)'
%          type "inf".
%    4) k1 and k2 - degree of s in the numerators of characteristic functions.
%    5) Number of the attenuation zeroes in the passband 
%       (they are equal for the both transfer functions).
%    6) Attenuation zeroes (initial values in Hz) in the passband - each on a separate row.
%    7) Number of attenuation poles (in the stopband) of the first function.
%    8) Attenuation poles in the stopband of the first function (initial valuesin Hz) - 
%       each on a separate row.
%    9) Number of attenuation poles (in the stopband) of the second function.
%   10) Attenuation poles in the stopband of the second function (initial values in Hz) - 
%       each on a separate row.
%   11) Maximal inequality for attenuation maximas in the passband (in dB) and 
%       maximal inequality for the minimas of the reserves of attenuation 
%       in the stopband (in dB) - on one row.

% Open the input file
FID = fopen(InFile,'rt');
if (FID < 0)
   fprintf('\n\r%s%s%s\n\r','File "',InFile,'" not found'); Ok = 0;
   return;
end;

% Reading the input file
% passband edge
SS = fgets(FID); 
A = str2num(SS); 
Fpass = A(1); DA = A(2);
% stopband specifications
SS = fgets(FID); N = str2num(SS); 
As = zeros(2,2*N);
for k = 1:N,
   SS = fgets(FID); A = str2num(SS);
   As(1,2*k-1) = A(1); As(2,2*k-1) = A(2);
   As(1,2*k) = A(3); As(2,2*k) = A(4);
end;
% k1 and k2
SS = fgets(FID);
A = str2num(SS); k1 = A(1); k2 = A(2);
% attenuation zeros
SS = fgets(FID); N = str2num(SS);
F0 = [];
for k = 1:N,
   SS = fgets(FID); F0 = [F0;str2num(SS)];
end;
% attenuation poles of the first function
SS = fgets(FID); N = str2num(SS);
Finf1 = [];
for k = 1:N,
   SS = fgets(FID); Finf1 = [Finf1;str2num(SS)];
end;
% attenuation poles of the second function
SS = fgets(FID); N = str2num(SS);
Finf2 = [];
for k = 1:N,
   SS = fgets(FID); Finf2 = [Finf2;str2num(SS)];
end;
% Inequalities
SS = fgets(FID); A = str2num(SS);
dDA = A(1); dAs = A(2); 
fclose(FID); Ok = 1;
