function CV = ChFncVal(C,n,Fz,Nz,Fi,Ni,F)
% CV = ChFncVal(C,n,Fz,Nz,Fi,Ni,F)
%     Calculate the value of Characteristic Function for frequency F
%     n is the filter order
%     The Characteristic function is for n even
%     K(F) = C * (Fz1^2 - F^2)^Nz1 * (Fz2^2 - F^2)^Nz2 * ... /
%                (Fi1^2 - F^2)^Ni2 * (Fi2^2 - F^2)^Ni2 * ...
%     and for odd n
%     K(F) = C * F * (Fz1^2 - F^2)^Nz1 * (Fz2^2 - F^2)^Nz2 * ... /
%                    (Fi1^2 - F^2)^Ni2 * (Fi2^2 - F^2)^Ni2 * ...
%     CV - calculated value 

CV = C;
if rem(n,2) ~= 0 
   CV = CV * F;
end;
for I = 1:length(Fz),
   CV = CV * (Fz(I)^2 - F^2)^Nz(I);
end;
for I = 1:length(Fi),
   CV = CV / (Fi(I)^2 - F^2)^Nz(I);
end;
