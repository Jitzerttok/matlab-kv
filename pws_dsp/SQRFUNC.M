function y = sqrfunc(x,a2,a1,a0)
%n = length(varargin);
%a2 = varargin{1};
%a1 = varargin{2}(1);
%a0 = varargin{3}(1);
y = a2 * x^2 + a1 * x + a0;
