function PS = PolFacToSum(PF)
% function PS = PolFacToSum(PF)
% Transforms a polynomial, represented as second order multiplier
% into form as sum.
% The polynomial as a product from second order factors has the form
% P(s) = k * (p11*s^2 + p12*s + p13) * (p21*s^2 + p22*s + p23) *  
%           *  ...   * (pn1*s^2 + pn2*s + pn3)
% and its coefficients are writen in a rectangular matrix with n+1 rows
% and 3 columns. In one row are the coefficients of one multiplier in
% order, corresponding to their indices above. First order multiplier 
% is treated as second order with p1k = 0. The n+1 row is for the constant 
% k and has following entries: 0  0  k. The polynomials as sum has the form
% P(s) = p1*s^n + p2*s^(n-1) + .... + pn
% and is written in one dimensional matrix in the usual for the Matlab form.

N = size(PF);
PS = 1;
for I = 1:N(1)
   if PF(I,1) == 0
      for J = 1:2, Q1(J) = PF(I,J+1); end;
      if Q1(1) == 0 PS = PS * Q1(2); 
      else PS = conv(PS,Q1); end;
   else
      for J = 1:3, Q2(J) = PF(I,J); end;
      PS = conv(PS,Q2); end;
  end;   