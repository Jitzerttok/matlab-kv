function [X, kX] = FreqOrder (Finf1n, Finf2n);
% function [X, kX] = FreqOrder (Finf1, Finf2);
% Ordering the frequencies if zero and infinite attenuation in 
% one array X - this is array with variables
% Input parameters:
%    Finf1 - normalised frequencies of infinite attanuation 
%            from the first transfer function
%    Finf2 - normalised frequencies of infinite attanuation 
%            from the second transfer function
% Output parameters:
%    X - array with all frequencies in increasing order by their values
%    kX - index array, it shows for each frequency in X which type is:
%         1 - frequency of infinite attenuation of the first function, 
%         2 -  frequency of infinite attenuation of the second function.
X = Finf1n;
kX = ones(length(Finf1n),1);
for k = 1:length(Finf2n),
   kk = 1;
   while (kk <= length(X)) & (X(kk) < Finf2n(k)), kk = kk + 1; end;
   X = [X(1:(kk-1)); Finf2n(k); X(kk:length(X))];
   kX = [kX(1:(kk-1)); 2; kX(kk:length(kX))];
end;
