function RA = Reserve_Att_1 (F, N, F0, Finf, eps, Fp, da, As)
% function RA = Reserve_Att_1 (F, N, F0, Finf1, eps, Fp, da, As)
% Returns the reserve in the attenuation. In the passband this is the difference
% between da and the filter attenuation, in the stopband - the difference between 
% filter attenuation and the specified minimal attenuation for this frequency.
% F - the frequency.
% N1 - degrees of the transfer functions.
% F0 contains the frequencies in the numerator of the char. function.
% Finf contains the frequencies in the denominator of the first char. function.
% eps is the ripple factor
% Fp - the upper limit of the passband.
% da - maximal ripple in the passband (in Np).
% As - specifications in the stopband. In the first row are the frequencies, 
%      in the second - attenuations (in Np). To every part corresponds two 
%      columns - for the left bound and for the right bound.

Nz(1:length(F0)) = 1; Ninf(1:length(Finf)) = 1; 
if (F < inf)
   K1 = ChFncVal(eps,N,F0,Nz,Finf,Ninf,F); 
   A = (1 + K1*K1); A = 0.5 * log(A);
   if F <= Fp RA = da - A;
   else
      k = 2;
      while As(1,k) < F, 
         k = k + 2; end;
      if As(1,k) == inf AA = As(2,k);
      else 
         AA = As(2,k-1) + (F-As(1,k-1)) * (As(2,k)-As(2,k-1)) / (As(1,k)-As(1,k-1)); 
      end; 
      RA = A - AA;
   end;
else
   RA = inf;
end;
