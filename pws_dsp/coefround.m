function [aq,nfa] = coefround(a,bits,type)
% This program normalize and quantize a given vector a off coefficients
%by rounding to a desired wordlenght w


if nargin == 2
   type = 'rounding';   
end

if max(abs(a))<1
   disp('All coefficients are lower than 1.Is not necessary to notmalize them. They are the same!');
   aq = a;
   nfa = 1;
else 
   f=log(max(abs(a)))/log(2);       		%Normalization of the vector a by
	n=2^ceil(f);                     		%n, a power of 2, such that
   an=a/n;				                 		%1>=an>=-1
   if strcmp(type,'rounding')
      aq = a2dR(an,bits);						%Quantization of an such that  1>aq>=-1
   end
   if strcmp(type,'truncation')
      aq = a2dT(an,bits);						%Quantization of an such that  1>aq>=-1
   end
   nfa=n;                           		%Normalization factor
end


