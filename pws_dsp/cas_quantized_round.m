function [bq,aq] = cas_quantized_round(b0,B,A,bits);

l_B=size(B);
l_A=size(A);

nfB=zeros(1,l_B(2)-1);
nfA=zeros(1,l_A(2)-1);

Bn=zeros(l_B(1),l_B(2));
An=zeros(l_A(1),l_A(2));

Bq=zeros(l_B(1),l_B(2));
Aq=zeros(l_A(1),l_A(2));

for i = 1:l_B(1)
    [Bn(i,:),nfB(i)]=scaling(B(i,:));
    [An(i,:),nfA(i)]=scaling(A(i,:));
    Bq(i,:) = a2dr(Bn(i,:),bits);
    Aq(i,:) = a2dr(An(i,:),bits);
end

b0q=b0*prod(nfB)/prod(nfA);
[bq,aq]=cas2dir(b0q,Bq,Aq);

end