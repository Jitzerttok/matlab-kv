function [A,B,Fn] = LoadSynfil(FileName,How)
% [A,B,Fn] = LoadSynfil(FileName,How)
%     Loads the transfer function in s from a file, created from the program Approx
%     FileName is a string, containig the name of the file with a full path name.
%     In A and B the function returns the numerator and denominator 
%       of the transfer function.
%     In Fn returns the normalization frequency.
%     How controls the manner in which the transfer function is in A and B. 
%       If How = 0 or How is not passed to the function, the polynomials in A and B are 
%         in sum form and are written in usual way for Matlab - 
%         from highest degree to lowest.
%       If How > 0 polynomials in A and B are in factorised form. 
%         In that case A and B are matrices 3 * N, where N is the number of 
%         multipliers of every polynomial. Every one column of these matrices contains 
%         one multiplier of the polynomial, written in usual form for the Matlab -
%         from highest degree to lowest.

if (nargin == 1) How = 0; end;
[C,D,Fn] = LoadSynfilMult(FileName);
if (isempty(C) == 1) & (isempty(D) == 1) return; end;
if (How > 0) A = C; B = D; return; end;
A = 1;
[Rw,Col] = size(C);
for k = 1:Rw
   i = 1; 
   while (C(k,i) == 0) i = i +1; end;
   j = 1; X = C(k,i);
   while (i < 3) i = i + 1; X = [X,C(k,i)]; end;
   A = conv(A,X); clear X;
end;
B = 1;
[Rw,Col] = size(D);
for k = 1:Rw
   i = 1; 
   while (D(k,i) == 0) i = i +1; end;
   j = 1; X = D(k,i);
   while (i < 3) i = i + 1; X = [X,D(k,i)]; end;
   B = conv(B,X); clear X;
end;

function [C,D,Fn] = LoadSynfilMult(FileName)
% Loads the transfer function in s from a file, created from the program Approx
% and returns the polynomials in factorised form.
Fn = 0; C = []; D = [];
fid = fopen(FileName,'rt'); 
if (fid == -1) fprintf('\n\r%s\n\r','File not found'); return; end;
[ss,count] = fscanf(fid,'%s',1);
[ss,count] = fscanf(fid,'%s',1); Fn = str2num(ss);  %ss is a string
[ss,count] = fscanf(fid,'%s',1); NC = str2num(ss);
[ss,count] = fscanf(fid,'%s',1); ND = str2num(ss);
n = 1; 
for k = 1:NC
   C = [C; 0 0 0];
   for i = 1:3 
      [ss,count] = fscanf(fid,'%s',1); 
      C(n,4-i) = str2num(ss); 
   end;
   [ss,count] = fscanf(fid,'%s',1); M = str2num(ss);
   m = n + 1;
   if (M > 1)
      for i = 1:M-1,
         C = [C; 0 0 0];
         for i = 1:3 C(i,m) = C(i,n); end;
         m = m + 1;
      end;
   end;
   n = m;
end;
n = 1;
for k = 1:ND
   for i = 1:3 
      [ss,count] = fscanf(fid,'%s',1); D(n,4-i) = str2num(ss); 
   end;
   [ss,count] = fscanf(fid,'%s',1); M = str2num(ss);
   m = n + 1;
   if (M > 1)
      for i = 1:M-1,
         for i = 1:3 D(i,m) = D(i,n); end;
         m = m + 1;
      end;
   end;
   n = m;
end;
fclose(fid);            
         
         






   

