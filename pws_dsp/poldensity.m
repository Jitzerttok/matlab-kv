function polesdensity(bits)

%bits are the bits of quantization 
%this function plot the possible poles location

%za 3 bits i pri i, i pri j e 0.1250
%za 4 bits i pri i, i pri j e 0.1250

if (nargin==1)
   for m=-2:1/2^(bits):2
   for n=-1:1/2^(bits):1
      a=[1,m,n];
      p=roots(a);
      y=0;
      p1=real(p(1));
      if ((p1>=-1)&(p1<=1))
         if (imag(p(1))==0)
            if (p1==sqrt(abs(n))&(p1~=0)&(m~=0))
            	plot(p(1),y,'r.');
               hold on;
               plot(-p(1),y,'r.');
               hold on;
            end
            
      		else
         		if (p1<sqrt(abs(n)))
            		y1=sqrt(n-(p1)^2);
            		x1=p1;
            		x2=-x1;
            		y2=-y1;
            		plot(x1,y1,'r.');
            		hold on;
            		plot(x2,y1,'r.');
            		hold on;
            		plot(x1,y2,'r.');
            		hold on;
            		plot(x2,y2,'r.');
            		hold on;

		            axis([-1 1 -1 1])
   		         axis square;
      	 	  end
            end 
         end
      end
   end

elseif disp('Too many input arguments')   
end
