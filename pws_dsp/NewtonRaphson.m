function [X, F] = NewtonRaphson (X, eps, FUNC, JACOBIAN, CHANGEARG, varargin)
% function [X, F] = NewtonRaphson (X, eps, FUNC, JACOBIAN, CHANGEARG, varagin)
%
% Solves a system of nonlinear equation fi(X) = 0, where i = 1,2,... n,  
% and X is n-dimensional vector, by Newton-Raphson Method. 
% Input data:
%   X - initial values for function arguments;
%   eps - accuracy in zeroing of the functions. Iterations stop when
%      all funcions are less or equal to eps (by absolute value).
%   FUNC - name of the m-file, containing the function, which 
%      calculates the functions fi. This function must be called
%      in the following manner: F = FUNC(X,par1,par2,...) where X is
%      the arguments and par1,par2, etc. are aditional parameters, 
%      necessary for calculations.
%   JACOBIAN - name of the m-file, containing the function, which
%      computes the Jacobian matrix [J] = [dfi/dxj], i,j =1,2,...n.
%      It must be caaled as follows: [J] = JACOBIAN(X,F,par1,par2...)
%      where J is the Jacobian (n x n matrix), X is the vector of 
%      function arguments, F - vector, containing the function values, 
%      par1, par2, etc. are additional parameters, necessary for 
%      calculations.
%   CHANGEARG - name of the m-file, containing the function, which 
%      changes the argument values. It is called as follows: 
%      [X] = CHANGEARG(X,dX,par1,par2,..), where in X are 
%      the function arguments, dX are their changes, par1, par2,... 
%      are paremeters and in varargout are the same paremeters 
%      as in varagin.
%   varargin are additional parameters that are necessary for 
%      all computations of functions, Jacobian and argument changing.
%      To all functions FUNC, JACOBIAN and CHANGEARG must be passed 
%      all parameters, listed in varargin.
% Output data:
%   X - the solution for function arguments
%   F - function values for the solution point.

while 1 == 1,
   F = feval (FUNC,X,varargin{:});
   k = 1;
   while k <= length(F),
      if abs(F(k)) > eps break; end;
      k = k + 1;
   end;
   if k > length(F) break; end;
   DFDX = feval(JACOBIAN,X,F,varargin{:});
   dX = - inv(DFDX) * F;
   X = feval(CHANGEARG,X,dX,varargin{:});
end;

