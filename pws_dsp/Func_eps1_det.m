function A = Func_eps1_det (eps1)
% Determination of minimal attenuation in stopband at the eps1 determination
% A - minimal attenuation (with reversed sign)
% eps1 
% N1, N2 - degrees of both transfer functions
% F0 - frequencies of zero attenuation
% Finf1, Finf2 - frequencies of infinite attenuation of the both functions
% DA - maximal ripple in the passband
% As - attenuations specifications in the stopband
% dDA, dAs - accuracies in the passband and in the stopband

% Definition of global variables
global DA As F0 Finf1 Finf2 dDA dAs N1 N2;
global eps1det eps2det N_eps1_det;

if isempty(N_eps1_det) == 1 N_eps1_det = 1;
else N_eps1_det = N_eps1_det + 1; end;
if N_eps1_det < 5 
   eps1det = [eps1det;eps1];
   if N_eps1_det == 1 eps2 = 1;
   else eps2 = eps2det(length(eps2det)); end;
else
   if eps1 < eps1det(2) NN = 2; 
   else NN = 3; end;
   eps1det = eps1det((NN-1):(NN+1)); eps2det = eps2det((NN-1):(NN+1));
   eps2 = interp1(eps1det,eps2det,eps1,'spline');
   eps1det = [eps1det(1:(NN-1));eps1;eps1det(NN:3)];
end;
   
[eps2,Fc,Ac] = eps2Determination(eps1,eps2);
if imag(eps2) ~= 0 A = inf; eps2 = inf;
else
   k = 1;
   while (Fc(k) <=1) k = k + 1; end;
   A = -min(Ac(k:length(Ac)));
end;

if N_eps1_det < 5 eps2det = [eps2det;eps2];
else eps2det = [eps2det(1:(NN-1));eps2;eps2det(NN:3)]; end;

dBNp = 20/log(10);
fprintf ('eps1 = %12.7f    eps2 = %12.7f   min.att = %10.5f\n',eps1,eps2,-A*dBNp);


