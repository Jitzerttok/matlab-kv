function [p, q, e] = TransferFuncS (L, eps, F0, Finf, pol_mode)
% function [p, q, e] = TransferFuncS (L, eps, F0, Finf, pol_mode)
% Calculates the polynomials p(s), q(s) and e(s) from 
% attenuation poles and zeroes. 
% Input data
%   L - degree of multiplier s^L in the characteristic function. 
%       If L > 0 this multiplier is in the numerator of the char.function;
%       if L < 0 the multiplier is in the denominator.
%   eps - the ripple factor
%   F0 - contains the frequencies of zero attenuation
%   Finf - contains the frequencies of infinite attenuation
%   pol_mode - determine the mode of the output polynomials; 
%              can be 'sum' if the polynomials must be as sums,
%              'fact' if the polynomials must be as factors.
% Output data
%   p(s) - numerator of the transfer function;
%   q(s) - numerator of the characteristic function;
%   e(s) - denominator of the transfer function.
% When the polinomials are in factorized form, only the first from the
% factors is multiplied with the constant multiplier.
% All frequencies must be normalised.

% Determination p(s) and q(s) as sums
pF = []; 
for k = 1:length(Finf); pF = [pF; 1 0 Finf(k)^2]; end;
while L < -1 
   pF = [1 0 0; pF]; L = L + 2; 
end;
if L < 0 pF = [0 1 0; pF]; end;
pS = PolFacToSum(pF);
qF = [];
for k = 1:length(F0); qF = [qF; 1 0 F0(k)^2]; end;
while L > 1 
   qF = [1 0 0; qF]; L = L - 2; 
end;
if L > 0 qF = [0 1 0; qF]; end;
for k = 1:3, qF(1,k) = eps * qF(1,k); end;
qS = PolFacToSum(qF);

% q(s)*q(-s) and p(s)*p(-s)
pSS = pS;
kk = 0;
for k = length(pSS):-1:1,
   if kk == 1 pSS(k) = -pSS(k); end;
   kk = 1 - kk;
end;
pSS = conv(pS,pSS);
NpSS = 0; kk = 0;
for k = 1:length(pSS),
   if kk == 0 NpSS = NpSS + 1; pSS(NpSS) = pSS(k); end;
   kk = 1 - kk;
end;
pSS = pSS(1:NpSS);
qSS = qS;
kk = 0;
for k = length(qSS):-1:1,
   if kk == 1 qSS(k) = -qSS(k); end;
   kk = 1 - kk;
end;
qSS = conv(qS,qSS);
NqSS = 0; kk = 0;
for k = 1:length(qSS),
   if kk == 0 NqSS = NqSS + 1; qSS(NqSS) = qSS(k); end;
   kk = 1 - kk;
end;
qSS = qSS(1:NqSS);

% Hurvitz polynomial
while length(qSS) < length(pSS) qSS = [0,qSS]; end;
while length(pSS) < length(qSS) pSS = [0,pSS]; end;
eSS = pSS + qSS;
R = roots(eSS);
NR = 0;
for k = 1:length(R),
   if imag(R(k)) >= 0 NR = NR + 1; R(NR) = R(k); end;
end;
R = R(1:NR);
eF = [];
for k = 1:NR,
   if imag(R(k)) == 0
      eF = [eF; 0 1 sqrt(R(k))];
   else
      fi = abs(angle(R(k))) / 2; Z = abs(R(k));
      eF = [eF; 1  2*cos(fi)*sqrt(Z) Z];
   end;
end;
H = sqrt(abs(eSS(1)));
for k = 1:3, eF(1,k) = eF(1,k) * H; end;
eS = PolFacToSum(eF);

if pol_mode == 'sum'
   p = pS; q = qS; e = eS;
else
   p = pF; q = qF; e = eF;
end;

      

   

