function [bq,aq] = par_quantized_trunc(C,B,A,bits);

l_B=size(B);
l_A=size(A);
l_C=size(C);

nfB=zeros(1,l_B(2)-1);
nfA=zeros(1,l_A(2)-1);
nfC=zeros(1,l_C(2)-1);

Bn=zeros(l_B(1),l_B(2));
An=zeros(l_A(1),l_A(2));
Cn=zeros(l_C(1),l_C(2));

Bq=zeros(l_B(1),l_B(2));
Aq=zeros(l_A(1),l_A(2));
Cq=zeros(l_C(1),l_C(2));


for i = 1:l_B(1)
    [Bn(i,:),nfB(i)] = scaling(B(i,:));
    [An(i,:),nfA(i)] = scaling(A(i,:));
    Bq(i,:) = a2dt(Bn(i,:),bits);
    Bq(i,:) = nfB(i)*Bq(i,:);
    Aq(i,:) = a2dt(An(i,:),bits);
    Aq(i,:) = nfA(i)*Aq(i,:);
end

for i = 1:l_C(1)
    [Cn(i,:),nfC(i)] = scaling(C(i,:));
    Cq(i,:) = a2dt(Cn(i,:),bits);
    Cq(i,:) = nfC(i)*Cq(i,:);
end


[bq,aq] = par2dir(Cq,Bq,Aq);

end