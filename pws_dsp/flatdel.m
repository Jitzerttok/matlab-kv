function c = flatdel(n,t0);
%
% Design of an allpass with maximally flat delay
% ----------------------------------------------
%
% c = flatdel(n,t0) calculates the denominator polynomial of
% the transfer function of an allpass of degree n, the group-
% delay of which approximates the desired value t0 maximally
% flat at omega = 0.
%
% Lit.: Thiran,J.P.: Trans. on CT-18 (1971) pp.659-664
%       Fettweis,A.: Trans.on AU-20 (1972), pp.112-114
%       Selesnick,I.W.: Trans.on CAS II-46 (1999), pp.40-50
%       Schuessler,H.W.: DSVII, Sect.3.6.3.4
%
% Example: n = 6; t0 = 2.5;

nue = 0:n-1;
c = cumprod([1, (nue-n).*(t0+nue-n)./(nue+1)./(t0+1+nue)]);
