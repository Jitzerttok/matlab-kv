function V = FacPolyVal(P,X)
% function V = FacPolyVal(P,X)
% Calculates the value V of the polynomial P, reperesented as product,
% for the complex argument X

V = 1; NP = size(P);
for I = 1:NP(1), 
   V = V * (P(I,1)*X*X + P(I,2)*X + P(I,3)); 
end;
