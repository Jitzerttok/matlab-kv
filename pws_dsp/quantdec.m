function [ dec_q ] = quantdec( dec,bit )
% 'quantdec' performs quantization for a given decimal number 'dec' into arbitrary number of bits.
% 
%   Detailed explanation goes here
a_int=dec2bin(dec) - '0';
dec_float = dec- floor(dec);
a_fload = dectobin(dec_float);
[break_,a_fload_q] = quantbin(a_fload,bit);
if and(strcmp(break_,'zero'),floor(dec)==0)
    a = a_fload;
else
    a=cat(2,a_int,a_fload);
end

[break_1,a_q] = quantbin(a,bit);

b_int = [];
if floor(dec)==0
    b_int = 0;
else
    for i=1:length(dec2bin(dec))
        b_int(i)=a_q(i);
    end
end
b_float = [];
if and(strcmp(break_,'zero'),floor(dec)==0)
for i=1:length(a_q)
    b_float(i) = a_q(i);
end
else
for i=length(dec2bin(dec))+1:length(a_q)
    b_float(i-length(dec2bin(dec))) = a_q(i);
end 
end

str_b_int = dec2hex(b_int)';

dec_b_int = bin2dec(str_b_int);
dec_b_float = bintodec(b_float);
dec_q = dec_b_int + dec_b_float;
display(dec)
display(a)
display(dec_q)
display(a_q)


end

