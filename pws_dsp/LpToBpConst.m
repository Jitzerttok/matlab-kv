function [Abp,Bbp] = LpToBpConst(Alp,Blp,Fc,Fl,Fu);
% function [Abp,Bbp] = LpToBpConst(Alp,Blp,Fc,Fl,Fu)
%     Performs low-pass to band-pass Constantinides transformation (in z-domain).
%     Abp and Bbp are numerator and denominator of the computed band-pass transfer function
%     Alp and Blp are numerator and denominator of the low-pass transfer function
%     All polynomials are written from the highest degree to lowest (from 0 to the 
%        highest negative degree of z)
%     Fc is the passband cutoff frequency of the low-pass filter
%     Fl and Fu are the lower and upper passband cutoff frequencies of the band-pass filter

% Calculation of alfa1 and alfa2 according to Ingle and Proakis, p.352
M = length(Alp) - 1; N = length(Blp) - 1;
NN = N; 
if (NN < M), NN = M; end;
K = tan(0.5*Fc) / tan(0.5*(Fu-Fl));
beta = cos(0.5*(Fu+Fl)) / cos(0.5*(Fu-Fl));
alfa1 = 2 * beta * K / (K + 1);
if abs(K-1) < 1e-6, alfa2 = 0; 
else alfa2 = (K - 1) / (K + 1); end;

% Clearing the arrays for bandpass transfer function
Abp = zeros(1,2*NN+1); Bbp = zeros(1,2*NN+1); 

% Forming the allpass transformation function z^(-1) = P(z)/Q(z)
if K == 1, P = [0 alfa1 -1]; Q = [1 -alfa1];
else P = [-alfa2 alfa1 -1]; Q = [1 -alfa1 alfa2]; end;

% Computing the bandpass transfer function
for k = 0:NN,
   C = PolyInPower(P,k); D = PolyInPower(Q,NN-k);
   C = conv(C,D);
   if k <= M, 
      for i=1:length(C), Abp(i) = Abp(i) + Alp(k+1)*C(i); end;
   end;
   if k <= N, 
      for i=1:length(C), Bbp(i) = Bbp(i) + Blp(k+1)*C(i); end;
   end;
end;
   
      


