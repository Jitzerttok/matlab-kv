function [n,mindP,maxomP] = ParHbellip(minomP,maxdP);

% Calculating the Parameters of an elliptic Halfbandfilter
% --------------------------------------------------------
% Starting with the tolerated lower limit minomP of the cutoff
% frequency of the lowpass and the upper limit maxdP of the
% acceptable deviation of the frequency response in the passband
% the program
%         [n,mindP,maxomP] = ParHbellip(minomP,maxdP)
% calculates first the required degree n of the system and in
% addition the extremal values maxomP and mindP, obtainable
% with the filter of this degree n. For the next step a value 
% omP <= maxomP has to be chosen. Starting with the degree n
% and this value omP the program Hbellip.m designs the halfband
% system, yielding a result with dP <= maxdP. If a solution
% with dP >= mindP is wanted the corresponding cutoff frequency
% omP >= minomP has to be calculated first with the program
% omPHbellip.m. Again the final step is the design with Hbellip.m.
%
% Input and output parameters:
%
% minomD   : minimum of the desired cutoff frequency/pi;
% maxdP    : maximum of the tolerated deviation;
% n        : necessary degree of the system;
% mindP    : minimum of dP, obtainable with omP = minomP
%            and degree n
% maxomP   : maximum of omP, obtainable with dP = maxdP 
%            and degree n;
%
% Lit.: Schuessler, Steffen AEU
%       Schuessler DSV2, Section 3.8.3;
%
% Example: minomP = .4; maxdP = .0005;

etaP = tan(minomP*pi/2); etaS = 1/etaP;         % Preparations;
kappa0 = 1/etaS^2; kappa0_ = sqrt(1-kappa0^2);  % Introducing the
m0 = kappa0^2; m0_ = 1-m0;                      % moduls

Delta = (1-maxdP)/sqrt(2*maxdP- maxdP^2);
kappa1 = 1/Delta^2; kappa1_ = sqrt(1-kappa1^2);
m1 = kappa1^2; m1_ = 1-m1;

K0 = ellipke(m0); K0_ = ellipke(m0_);            % Calculation of
K1 = ellipke(m1); K1_ = ellipke(m1_);            % the required
n = ceil(K0*K1_/(K0_*K1));                       % degree

mu = 1:floor(n/2);                               % Preparation 
                                                 
if rem(n,2) == 0;
    [sn0,cn0,dn0] = ellipj((2*mu-1)*K0/n,m0);
    [sn1,cn1,dn1] = ellipj((2*mu-1)*K1_/n,m1_);
else
    [sn0,cn0,dn0] = ellipj(2*mu*K0/n,m0);
    [sn1,cn1,dn1] = ellipj(2*mu*K1_/n,m1_);
end;

kappa11 = kappa0^n*prod(cn0.^4./dn0.^4);          % Calculating
Delta1 = sqrt(1/kappa11);                         % mindP;
mindP = 1 - Delta1/sqrt(1+Delta1^2);

kappa01_ = kappa1_^n*prod(cn1.^4./dn1.^4);        % Calculating
kappa01 = sqrt(1-kappa01_^2);                     % maxomP
maxetaP = sqrt(kappa01);
maxomP = 2*atan(maxetaP)/pi;


