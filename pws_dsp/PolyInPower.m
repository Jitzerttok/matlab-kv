function P = PolyInPower(A,N);
% function P = PolyInPower(A,N);
%    Calculates the N-th power of the polynomial A;
P = 1;
for K = 1:N, P = conv(P,A); end;