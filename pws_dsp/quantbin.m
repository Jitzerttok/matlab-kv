function [break_,quant]=quantbin(bin,bit)
zeros=0;
ones=0;
zerobreak=0;
onebreak=0;
quant=[];

total_zeros = 0;
total_ones = 0;
for i=1:length(bin)
    if (bin(i)==0)
        total_zeros = total_zeros + 1;
    else
        total_ones = total_ones + 1;
    end
end

for i=1:length(bin)

    if or(and(zeros>bit,ones==bit),and(total_zeros==0,ones==bit))
        onebreak = 1;
    end
    if or(and(ones>bit,zeros==bit),and(total_ones==0,zeros==bit))
        zerobreak = 1;
    end

    if (bin(i)==0)
        if zerobreak
            break_= 'zero';
            break;
        end
        quant(i)=0;
        zeros=zeros+1;
    else if (bin(i)==1)
            if onebreak
                break_='one';
                break;
            end
        quant(i)=1;
        ones=ones+1;
        end
    end
end
display(quant)