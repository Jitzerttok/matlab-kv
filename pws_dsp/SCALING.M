function [an,nfa] = scaling(a)
% This program normalize a given vector a off coefficients

 f=log(max(abs(a)))/log(2);       %Normalization of the vector a by
 n=2^ceil(f);                     %n, a power of 2, such that
 an=a/n;                          %1>=an>=-1
 nfa=n;                           %Normalization factor