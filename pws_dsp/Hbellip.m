function [b1,b2,a,p,bn,z,dP_] = Hbellip(n,omP);

% Design of Elliptic (Cauer)- halfband-filters with degree n 
%------------------------------------------------------------------
% [b1,b2,a,p,bm,z,dP_] = Hbellip(n,omP) designs an elliptic (Cauer)
% halfband-filter of degree n with the normalized passband cutoff-
% frequency omP. The program starts with the design of a continuous
% Cauer lowpass with the passband cutoff frequency etaP and the stop-
% band frequency etaS = 1/etaP such that the deviation is minimized.
% The poles of its transfer function are located on the unit circle
% in the w-domain. The bilinear transformation into the digital domain 
% is done with the program bilinwz.m. Futhermore the numerator
% polynomial b2 of the transfer-function of the corresponding highpass
% is calculated. Finally the resulting deviation dP_ is determined.
% The mentioned subprogram biliwz is needed.
%
% Input and output parameters 
% n        : degree of the filter;
% omP      : Passband cutoff-frequency of the lowpass, normalized by pi.
% b1,b2    : numerator polynomials of HLp(z) and HHp(z);
% a        : denominator polynomial of HLp(z) and HHp(z);
% p,bm,n   : pole-zero representation of HLp(z);
% dP_      : maximal deviation of abs(HLp(om)) in the passband;
% 
% Lit.; Schuessler, Steffen AEU
%       Schuessler DSV2, Abschn. 3.8;
% Example: n = 5; omP = .4; see figures 9 d-f and 10 in the cited paper


etaP = tan(omP*pi/2);                       % Preparations
kappa0 = etaP^2; r = 2*etaP/(1+kappa0); 
K0 = ellipke(kappa0^2); Kr = ellipke(r^2);
i = 1:floor(n/2); l = floor(n/2)+1:floor(3*n/2);
              
if rem(n,2) == 0;                           % Design of the        
   sni = ellipj((2*i-1)*K0/n,kappa0^2);     % normalized
   wz = [j./(etaP*sni) -j./(etaP*sni)];     % continuous 
   [sn,cn] = ellipj((2*l-1)*Kr/n,r^2);      % lowpass 
else;                                       % with the
   sni = ellipj(2*i*K0/n,kappa0^2);         % cutoff-  
   wz = [j./(etaP*sni) -j./(etaP*sni)];     % frequency
   [sn,cn] = ellipj(2*l*Kr/n,r^2);          % etaP for even 
end;                                        % and odd           
wp = [cn+j*sn];                             % values of n

G1 = prod(j-wz)/prod(j-wp);                 % Bilinear
wb = sqrt(.5)/abs(G1);                      % transformation 
[p,bn,z] = biliwz(wp,wb,wz);                % into the  
b1 = bn*real(poly(z));                      % halfband-
a = real(poly(p));                          % lowpass;
b2 = (-1).^(0:n).*b1;                       % numerator-polynom
                                            % of HHp(z);
zP = exp(j*omP*pi);                         % Calculation of the
dP_ = 1-abs(polyval(b1,zP)/polyval(a,zP));  % deviation dP_;
