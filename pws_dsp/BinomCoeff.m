function [P] = BinomCoeff(N,S)
% P = BinomCoeff(N,S)
%     Returns the binomial coefficients of the (A+S*B)^N.
%     S must be +1 or -1.
if N == 0, P = 1;
else P =[1 1]; end;
for k = 2:N,
   for j = k:-1:2, P(j) = P(j) + P(j-1); end;
   P(k+1) = 1;
end;
if S < 0,
   for k = 2:2:N+1, P(k) = -P(k); end;
end;
