function Jac = Jacobian1 (X, N, F0, Finf, Fc, eps, DAn, Asn, where)
% function Jac = Jacobian1 (Fc, F0, Finf1, Finf2, eps1,eps2)
% Computes the Jacobian for the Remez1 method .
% Jac is the Jacobian. This is a square matrix which elements
% are equal to da/d(Omi). Omi - frequencies of zero or maximal 
% attenuation of the filter.
% X - array, containing the normalized frequencies of zero
%     and infinite attenuation.
% Ac - rezerve of attenuation in critical points (not used)
% F0 - frequencies of zero attenuation
% Finf - frequencies of infinite attenuation of the first function
% Fc - frequencies of critical points.
% eps - ripple factors for both transfer functions.
% DAn - maximal ripples (in Np) in passband.
% Asn - normalized specifications in stopband.
% where - shows where are the calculations: 'passband', 'stopband'

Jac = []; 
N0 = ones(length(F0)); Ninf = ones(length(Finf));
k = 1;
if strcmp(where,'passband') == 1
   while Fc(k) <= 1
      K1 = ChFncVal(eps,N,F0,N0,Finf,Ninf,Fc(k)); K1 = K1*K1;
      T1 = 1 / (1 + K1); 
      RJ = [];
      for kk = 1:length(F0),
         RJ = [RJ, -2*T1*K1*F0(kk) / (F0(kk)^2 - Fc(k)^2)];
      end;
      RJ = [RJ,-1];
      Jac = [Jac;RJ];
      k = k + 1;
   end;
else
   for k = 1:length(Fc)
      RJ = [];
      K1 = ChFncVal(eps,N,F0,N0,Finf,Ninf,Fc(k)); K1 = K1*K1;
      T1 = 1 / (1 + K1);
      for kk = 1:length(Finf),
         RJ = [RJ, -2*T1*K1*X(kk) / (X(kk)^2 - Fc(k)^2)];
      end;
      RJ = [RJ, -1];
      Jac = [Jac;RJ];
   end;
end;

