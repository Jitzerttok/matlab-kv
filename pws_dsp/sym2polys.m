function [c] = sym2polys(s,x)
%Sym2Polys  Extract the coefficients of a symbolic polynomial.
%           This function is an extension of the Matlab SYM2POLY function 
%           in that this function allows the coefficients to be symbolic.
%
%Usage: c = sym2polys(p,x)
%       where p is the (multi) symbolic polynomial and x is the
%       independent variable. If x is not specified then the
%       alphabetically last variable is used as the independent variable.
%
%Example:    If p = a*b*x^2 + b*c*x + c*d
%            then z = sym2polys(p) will generate
%            z = [a*b, b*c, c*d]
%
%            whereas z = sym2polys(p,'b') will generate
%            z = [a*x^2+c*x, c*d]
%
%see also: sym2poly, sym, syms, subs, numden, residue
%

% Collect has a bug in it
%     try sym2poly(collect(poly2sym([2 1]))) gives correct [2 1] as answer
% now try sym2poly(collect(poly2sym([  1]))) is an error
%
% now try sym2poly(poly2sym([2 1])) gives correct [2 1] as answer
% now try sym2poly(poly2sym([  1])) gives correct [  1] as answer
%
% error in collect is that it doesn't recognize any constant
% as constant*variable^0 when collecting powers of the variable.
%
% for this program, if p=2*x
% then collect(p/x) should give 2 but gives [] instead

% Paul Godfrey
% pgodfrey@conexant.com
% July 16, 2004

if ~exist('x', 'var')
   sv=findsym(s);
   if length(sv)>0
      x=sv(end);
   end
   if (x==sv | length(sv)==0)
      c=sym2poly(s);
      return
   end
end

if ~ismember(x,findsym(s))
   error([char(x), ' not found in ', char(s)]);
end

s=expand(s);
s=collect(s,x);
n=1;

while length(s)>0
   c(n)=subs(s,x,0);
   ss=s-c(n);
   s=collect(ss/x);
   n=n+1;
% workaround for collect bug
   if isempty(findsym(s))
%     keyboard
%     c(n)=expand(ss/x);
      c(n)=eval(ss/x);
%     c(n)=double(ss/x);
      c=fliplr(c);
      if c(1)==sym('0')
         c=c(2:end);
      end
      return
   end
end

c=fliplr(c);

return
