function [A1, B1, Q1, A2, B2, Q2] = Approx2AP (InFile)
% function [A1, B1, A2, B2] = Approx2AP (InFile);
% Realizes equiripple approximation of a LP filter. 
% The received transfer function is in the form as a product of 
% two transfer function, each of which can be realized az a sum of
% two allpass functions.
% A1 and B1 are the numerator and denominator in s of the first transfer function.
% A2 and B2 are the numerator and denominator in s of the second transfer function.
% All polynomials are in form as a product.
% 'InFile' is the name of the input text file, which contains the input data 

% Definition of global variables
clear DA As F0 Finf1 Finf2 dDA dAs N1 N2;
clear eps1det eps2det N_eps1_det;
global DA As F0 Finf1 Finf2 dDA dAs N1 N2;
global eps1det eps2det N_eps1_det;

% Reading the input file
[Fpass,DAi,Asi,k1,k2,F0i,Finf1i,Finf2i,dDAi,dAsi,Ok] = Read_In_File (InFile);
if Ok == 0 return; end;

% Normalization of input data
DA = DAi; As = Asi; F0 = F0i; Finf1 = Finf1i; Finf2 = Finf2i; 
dDA = dDAi; dAs = dAsi;
NormInData(Fpass);
N1 = k1 + 2 * length(F0); N2 = k2 + 2 * length(F0);

% Optimization
% Determination the upper limit for eps1
eps1u = 1;
N0 = ones(length(F0)); Ninf1 = ones(length(Finf1));
F00 = F0; Fi1 = Finf1; T = sqrt(10^(DAi/10) - 1);
[Fc,Ac,F00,Fi1] = Remez1(N1,F00,Fi1,eps1u,DA,As,dDA,dAs);
while abs(Ac(1)) > dDA 
   K = ChFncVal(eps1u,N1,F00,N0,Fi1,Ninf1,Fc(1)); 
   eps1u = eps1u * T / K;
   [Fc,Ac,F00,Fi1] = Remez1(N1,F00,Fi1,eps1u,DA,As,dDA,dAs);
end;

% Determination the interval of the uncertainty for eps1
eps1l = 0.05 * eps1u; deps = 0.05 * eps1u; eps1u = 0.95 * eps1u;
% optimization the eps1 value
[eps1,Acmin] = GoCu(eps1l,eps1u,deps,'Func_eps1_det');
k = 1;
while eps1det(k) ~= eps1 k = k + 1; end;
eps2 = eps2det(k);
[Fc, Ac, OK] = Remez2(eps1, eps2);
dBNp = 20/log(10);
fprintf ('\n\nSolution:');
fprintf ('\neps1 = %12.7f    eps2 = %12.7f   min.att = %10.5f\n',eps1,eps2,-Acmin*dBNp);
if eps1 == eps1u 
   fprintf('\nAttention: optimal solution for eps1 is at the upper bound of the search interval\n\n');
end;
fprintf ('\nFrequencies of zero att., Hz:');
for k = 1:length(F0), fprintf('%14.5f',F0(k)*Fpass); end;
fprintf ('\nFreq. of inf. att. of trans. func. 1, Hz:');
for k = 1:length(Finf1), fprintf('%14.5f',Finf1(k)*Fpass); end;
fprintf ('\nFreq. of inf. att. of trans. func. 2, Hz:');
for k = 1:length(Finf2), fprintf('%14.5f',Finf2(k)*Fpass); end;
fprintf('\n\n     Critical points \n Frequency, Hz    Rezerv of att., dB');
for k = 1:length(Fc),
   fprintf('\n%14.5f%18.5f',Fc(k)*Fpass,Ac(k)*dBNp);
end;

% Transfer functions
[p1,q1,e1] = TransferFuncS(k1,eps1,F0,Finf1,'fac');
[p2,q2,e2] = TransferFuncS(k2,eps2,F0,Finf2,'fac');
fprintf('\n\nTransfer function 1\nNumerator\n');
Sz = size(p1);
for k = 1:Sz(1),
   fprintf('    %14.6f*s^2  +  %14.6f*s  +  %14.6f\n',p1(k,1),p1(k,2),p1(k,3));
end;
fprintf('Denominator\n');
Sz = size(e1);
for k = 1:Sz(1),
   fprintf('    %14.6f*s^2  +  %14.6f*s  +  %14.6f\n',e1(k,1),e1(k,2),e1(k,3));
end;
fprintf('\nTransfer function 2\nNumerator\n');
Sz = size(p2);
for k = 1:Sz(1),
   fprintf('    %14.6f*s^2  +  %14.6f*s  +  %14.6f\n',p2(k,1),p2(k,2),p2(k,3));
end;
fprintf('Denominator\n');
Sz = size(e2);
for k = 1:Sz(1),
   fprintf('    %14.6f*s^2  +  %14.6f*s  +  %14.6f\n',e2(k,1),e2(k,2),e2(k,3));
end;
[A1,Q1,B1] = TransferFuncS(k1,eps1,F0,Finf1,'sum');
[A2,Q2,B2] = TransferFuncS(k2,eps2,F0,Finf2,'sum');
