function [b1,b2,a,p,bm,z,dP_] = HbBut(omP,dP);

% Design of a Butterworth Halfband-Filter
% ---------------------------------------
%
% [b1,b2,a,p,bm,z,dP_] = HbBut(omP,dP) designs a Butterwoth-Halfband-
% system of minimal degree with normalized cutoff frequency omP of the
% lowpass such that the deviation dP_ of abs(HLp(om)) at the point omP
% is not larger than the tolerated value dD.
%
% Input and output Parameters
% omP      : cutoff frequency of the lowpass, divided by pi;
% dP       : tolerated deviation at omP;
% b1,b2    : numerator polynomials of HLp(z) and HHp(z);
% a        : denominator polynomial of HLp(z) and HHp(z);
% p,bm,z   : Pol-zero representation of HLp(z);
% dP_      : obtained deviation 1-max(abs(HLp(om)) for om <= omP;
% 
% Lit,: Schuessler, Steffen AEU
%       Schuessler DSV2, section 3.8.3
%
% Example: omP = .4; dP = .02;
% see figures 9,a-c in the cited paper

                                              % Preparations:
Delta = sqrt(2*dP-dP^2)/(1-dP);               % Calculation of
n = ceil(log10(Delta)/log10(tan(omP*pi/2)));  % the required degree,
dP_ = 1- 1/sqrt(1+ (tan(omP*pi/2))^(2*n));    % the obtained dP_.

phi = (1+ (2*(1:n)-1)/n)*pi/2;                % Calculation of 
p = -j./tan(phi/2); a = real(poly(p));        % the denominator

z = -ones(1,n); b1 = poly(z);                 % Calculation of
bm = sum(a)/sum(b1); b1 = bm*b1;              % the numerators
b2 = (-1).^(0:n).*b1;
