function omP_ = omPHbellip(n,dP_);

%  Calculing the cutoff frequency omP_ of an elliptic halfband-filter
% ------------------------------------------------------------------
%
% omP_ = omPHbellip(n,dP_) calculates the normalized cutoff frequency
% omP_ of an elliptic halfband-filter with degree n with presribed
% maximal deviation dP_ in the passbands. The degree n has to be 
% calculated first with the program ParHbellip.m, which provides as
% well mindP, to be taken into account while choosing dP_.
%
% Input and output parameters
% n        : degree of the system;
% dP_      : desired cutoff-frequency, to be choosen >= mindP;
% omP_     : resulting cutoff-frequency of the lowpass;
%
% Lit.: Schuessler, Steffen AEU
%       Schuessler DSV2, Section3.8.3
%
% Example: n = 5; select dP_ between .00019 and .0005;


Delta = (1-dP_)/sqrt(2*dP_ -dP_^2);              % Preparations
kappa1 = 1/Delta^2; kappa1_ = sqrt(1-kappa1^2);
m1_ = kappa1_^2; K1_ = ellipke(m1_);

mu = 1:floor(n/2);                               % Calculations
if rem(n,2) == 0;                                % of the
    [sn1,cn1,dn1] = ellipj((2*mu-1)*K1_/n,m1_);  % modul,
else
    [sn1,cn1,dn1] = ellipj(2*mu*K1_/n,m1_);
end

kappa01_ = kappa1_^n*prod(cn1.^4./dn1.^4);       % the kappa01,
kappa01 = sqrt(1- kappa01_^2);

etaP_ = sqrt(kappa01);                           % the cutoff-
omP_ = 2*atan(etaP_)/pi;                         % frequency;