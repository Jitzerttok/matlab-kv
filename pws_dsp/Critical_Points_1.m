function [Fc,Ac,nPBCP] = Critical_Points_1(N,F0,Finf,eps,Fp,da,As,where,rel_acc)
% function Fc = Critical_Points_1 (F0, Finf1, Finf2, eps1, eps2, da, As)
% Finds all critical points and returns them in Fc
% Ac - contains rezerve of attenuation for each critical point
% nPBCP - number of critical points in the passband.
% N1 and N2 - degree of the transfer function
% F0 contains the frequencies in the numerator of the char. function.
% Finf contains the frequencies in the denominator of the char. function.
% eps is the ripple factor
% Fp - the upper limit of the passband.
% da - maximal ripple in the passband (in Np).
% As - specifications in the stopband. In the first row are the frequencies, 
%      in the second - attenuations (in Np). To every part corresponds two 
%      columns - for the left bound and for the right bound
% where - string defining in which band to compute the critical ponts: 'passband', 
%         'stopband', 'all'
% rel_acc - relative accuracy of determination of critical points

Fc = []; Ac = []; nPBCP = 0;

% Passband
if strcmp(where,'passband') + strcmp(where,'all') >= 1
   Fl = 0; 
   for k = 1:length(F0),
      Fu = F0(k);
      [Fnew,A] = GoCu(Fl,Fu,rel_acc*(Fu-Fl),'Reserve_Att_1',N,F0,Finf,eps,Fp,da,As);
      Fc = [Fc; Fnew]; Ac = [Ac; A];
      Fl = Fu;
   end;
   [Fnew,A] = GoCu(Fl,Fp,rel_acc*(Fp-Fl),'Reserve_Att_1',N,F0,Finf,eps,Fp,da,As);
   Fc = [Fc; Fnew]; Ac = [Ac; A]; 
   nPBCP = length(Fc);
end;

% Stopband
% Ordering frequencies of the infinite attenuation
if strcmp(where,'stopband') + strcmp(where,'all') >= 1
   Fi = Finf;
% Searching in the stop band
   kFi = 1; kk = 1;
   Farcl = 0.9999999*As(1,1); Fl = As(1,1); 
% Search in one arc
   while kFi <= (length(Fi) + 1),
      if kFi <= length(Fi) Farcr = Fi(kFi); else Farcr = inf; end; 
      Fnext = Farcl; Amin = inf;
      while Fnext < Farcr,
         if As(1,2*kk) < Farcr Fu = As(1,2*kk); kk = kk + 1; Fnext = Fu;
         else 
            Fnext = Farcr; 
            if Farcr < inf Fu = Farcr - 0.001*(Farcr-Fl); 
            else 
               if Fl == Farcl, Fu = 1.00001*Fl; else Fu = Fl; end;
               A = Reserve_Att_1(Fu,N,F0,Finf,eps,Fp,da,As); 
               while 1 == 1
                  Fu = 2 * Fu;
                  AA = Reserve_Att_1(Fu,N,F0,Finf,eps,Fp,da,As);
                  if AA >= A break; else A = AA; end;
               end;
            end;
         end;
         if Fl == Farcl Fl = Fl + 0.001 * (Fu - Fl); end;
         [Fnew,A] = GoCu(Fl,Fu,rel_acc*(Fu-Fl),'Reserve_Att_1',N,F0,Finf,eps,Fp,da,As);
         if A < Amin Amin = A; Fmin = Fnew; end;
         Fl = Fnext;
      end;
      Fc = [Fc; Fmin]; Ac = [Ac; Amin];
      Farcl = Farcr; kFi = kFi + 1;
      if As(1,2*kk) == Farcr kk = kk + 1; end;
   end;
end;