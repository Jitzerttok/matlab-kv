function [A,B] = LPToHPFac(a,b)
% function [a,b] = LPToHPFac(a,b)
% Low pass to high pass analog transformation on the function a(s)/b(s) (s -> 1/s).
% a(s) and b(s) are in factorized form.
% The constant multiplier always is included in b(s).
[A,MM,Ca,CMRa] = LP2HPPol(a); NA = size(A);
if (CMRa > 0) A = [A(1:(CMRa-1),1:3);A((CMRa+1):Na(1),1:3)]; end;
[B,NN,Cb,CMRb] = LP2HPPol(b);
NN = MM - NN;
for i = 1:abs(NN),
   if (NN > 0) A = [A; 0  1  0];
   else B = [B; 0  1  0]; end;
end;
Cb = Cb/Ca;
if (Cb ~= 1) 
   if (CMRb > 0) B(CMRb,3) = Cb;
   else B = [B; 0  0  Cb]; end;
end;

function [Q,L,C,CMR] = LP2HPPol(P)
% Performs LP to HP transform on one polynomial P (s -> 1/s).
% L - the degree of the multiplier s^(L) (usual L is negative)
% C - constant multiplier before P(s), due to transformation.
% CMR - the row in P, containing only constant multiplier, 
%      0 if there no exists such row.
NP = size(P); Q = [];
L = 0; C = 1; CMR = 0;
for i = 1:NP(1),
   if (P(i,3) == 0)
      if (P(i,2) == 0) L = L - 2; C = C * P(i,1);
      else L = L - 1; C = C * P(i,2); end;
   else
      if (P(i,1) == 0) 
         if (P(i,2) == 0) CMR = i; C = C * P(i,3); Q = [Q; 0 0 1];
         else Q = [Q; 0  1  P(i,2)/P(i,3)]; C = C * P(i,3); end;
      else 
         Q = [Q; 1  P(i,2)/P(i,3) P(i,1)/P(i,3)];         
         L = L - 2; C = C * P(i,3);
      end;
   end;
end;
