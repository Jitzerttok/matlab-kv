function polesdensity(bits)

%bits are the bits of quantization 
%this function plot the possible poles location

%za 3 bits i pri i, i pri j e 0.1250
%za 4 bits i pri i, i pri j e 0.1250

if (nargin==1)
   for i=-2:1/2^(bits):2
      for j=-1:1/2^(bits):1
      	a=[1,i,j];
      	p=roots(a);
      	[b,c]=scircle1(0,0,sqrt(j));
      
      	plot(roots(a));
      	hold on;
      	axis([-1 1 -1 1]);
      	axis square
		   plot(b,c)
      	hold on;
   	end
	end
elseif disp('Too many input arguments')   
end
