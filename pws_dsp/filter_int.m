function y = filter_int(A,B,x,degA,degB)
if degA > degB, kA = 2^(degB-degA); kB = 1; k = 2^(-degB); 
else kA = 2^(degA-degB); kB = 1; k = 2^(-degA); end;
[M,N] = size(x);
[M,NA] = size(A);
[M,NB] = size(B);
M = max(NA,NB);
for n = 1:(M-1), x = [0 x]; y(n) = 0; end;
for n = M:(N+M-1),
   yA = 0;
   for nn = 1:NA, yA = yA + round(x(n-nn+1)*A(nn)); end;
   yB = 0;
   for nn = 2:NB, yB = yB + round(y(n-nn+1)*B(nn)); end;
   y(n) = round(k*(round(kA*yA) - round(kB*yB)));
end;
for n = 1:N, 
   y(n) = y(n+M-1); 
end;
y = y(1:N);

      
      