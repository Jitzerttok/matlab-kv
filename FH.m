function F  = FH( C1, C2, fi, LH, lambda0 )
    tri = pi*LH*sin(deg2rad(fi))/lambda0;
    secx = @(t) sin(t)./t;
    F = C1*secx(tri) + C2*(secx(tri+pi/2) + secx(tri-pi/2));
end

