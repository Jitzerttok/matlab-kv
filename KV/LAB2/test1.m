N = 9;
Wo = 500;
Bw = 200;

[z,p,k] = buttap(N);
[Ns, Ds] = zp2tf(z, p, k);
[Nst, Dst] = lp2bp(Ns, Ds, Wo, Bw);

[T, w] = freqs(Ns, Ds);
a = -20*log10(abs(T));
fi = unwrap(angle(T)) * 180 / pi;

figure(1);
plot(w, a);
xlabel('??????? [rad/s]');
ylabel('????????? dB');
title('????????? ?? ????');
grid;

figure(2);
semilogx(w, fi);
xlabel('??????? [rad/s]');
ylabel('???? deg');
title('??? ?? ????');
grid;

[T, w] = freqs(Nst, Dst);
a = -20*log10(abs(T));
fi = unwrap(angle(T)) * 180 / pi;

figure(3);
plot(w, a);
xlim([0 1000]);
ylim([0 190]);
xlabel('??????? [rad/s]');
ylabel('????????? dB');
title('????????? ?? ????');
grid;

figure(4);
semilogx(w, fi);
xlim([0,1e4]);
xlabel('??????? [rad/s]');
ylabel('???? deg');
title('??? ?? ????');
grid;