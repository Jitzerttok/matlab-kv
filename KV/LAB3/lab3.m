b=[0.002 -0.006 0.011 -0.012 0.011 -0.006 0.002];
a=[1 -5.304 12.1098 -15.1949 11.0378 -4.3996 0.7521];

[b01, B1, A1]=dir2cas(b,a);
[b02, B2, A2]=dir2par(b,a);
[B3, A3]=dir2ladr(b,a);
%[B4, A4]=eucz(b,a);

n1=0;
n2=20;
stepf = stepseq(n1,n1,n2);
h=casfiltr(b01, B1, A1, stepf);
figure(1);
bar(n1:n2,h);
grid;
