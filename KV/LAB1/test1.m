close all
clear all
clc

Ns = [0 0 0 0 0 0.03];
Ds = [1 1.62 1.31 0.65 0.21 0.03];


fig1 = figure(1);
pzmap(Ns, Ds);


[T, w] = freqs(Ns, Ds);
m = abs(T);
fi = angle(T);

fig2 = figure(2);
subplot(211);
plot(w, m);
ylabel('���������');
xlabel('������� (rad/s)');
title('����������-�������� ��������������');
grid;

subplot(212);
plot(w, unwrap(fi)*180/pi);
ylabel('���');
xlabel('������� (rad/s)');
title('������-�������� ��������������');
grid;

fig3 = figure(3);
subplot(211);
loglog(w, m);
ylabel('���������');
xlabel('������� (rad/s)');
title('����������-�������� ��������������');
grid;

subplot(212);
semilogx(w, unwrap(fi)*180/pi);
ylabel('���');
xlabel('������� (rad/s)');
title('������-�������� ��������������');
grid;

fig4 = figure(4);
subplot(211);
plot(w, 20*log10(m));
ylabel('��������� � dB');
xlabel('������� (rad/s)');
title('����������-�������� ��������������');
grid;

subplot(212);
plot(w, unwrap(fi)*180/pi);
ylabel('���');
xlabel('������� (rad/s)');
title('������-�������� ��������������');
grid;


[imp, x, t] = impulse(Ns, Ds);

fig5 = figure(5);
subplot(211);
plot(t, imp);
ylabel('��������� (volts)');
xlabel('����� (s)');
title('�������� ��������������');
grid;

[st, x, t] = step(Ns, Ds);
subplot(212);
plot(t, st);
ylabel('��������� (volts)');
xlabel('����� (s)');
title('�������� ��������������');
grid;

mkdir exports
saveas(fig1,'exports/fig1.png');
saveas(fig2,'exports/fig2.png');
saveas(fig3,'exports/fig3.png');
saveas(fig4,'exports/fig4.png');
saveas(fig5,'exports/fig5.png');









