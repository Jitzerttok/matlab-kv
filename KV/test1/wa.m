close all
clear all
clc;

Nz=[0.156 -0.1 -0.03];
Dz=[-0.5 0.8];
[T,w]=freqz(Nz,Dz);

ma=20*log10(abs(T));
fi=unwrap(angle(T));
fig1 = figure(1);
subplot(2,2,1);
plot(w,ma);
xlabel('Frequency 0 -pi rad/s', 'FontName', 'Zapf Chancery'); ylabel('Amplitude', 'FontName', 'Zapf Chancery');
title('��� � dB', 'FontName', 'Zapf Chancery');
subplot(2,2,3);
plot(w,fi*180/pi);
xlabel('������� �� 0 �� � rad/s ', 'FontName', 'Zapf Chancery'); title('��� � �������', 'FontName', 'Zapf Chancery');
subplot(2,2,2);
[grd,w]=grpdelay(Nz,Dz);
plot(w,grd);
xlabel('������� �� 0 �� � rad/s', 'FontName', 'Zapf Chancery'); title('���', 'FontName', 'Zapf Chancery');
subplot(2,2,4);
zplane(Nz,Dz);

fig2 = figure(2);
x1=impseq(0,0,100);
n=[0:100];
subplot(2,1,1);
stem(n,x1);
title('D I', 'FontName', 'Zapf Chancery');
xlabel('������ ��� �������', 'FontName', 'Zapf Chancery');
subplot(2,1,2)
h=filter(Nz,Dz,x1);
stem(n,h);
title('� �', 'FontName', 'Zapf Chancery');
xlabel('������ ��� �������', 'FontName', 'Zapf Chancery');

fig3 = figure(3);
x2=stepseq(0,0,100);
n=[0:100];
subplot(2,1,1);
stem(n,x2);
title('�������� ����������', 'FontName', 'Zapf Chancery');
xlabel('������ �� �������', 'FontName', 'Zapf Chancery');
subplot(2,1,2);
g=filter(Nz,Dz,x2);
stem(n,g)
title('� �', 'FontName', 'Zapf Chancery');
xlabel('������ ��� �������', 'FontName', 'Zapf Chancery');

fig4 = figure(4);
n1=[0:100];
x3=(5+cos(1.5*pi*n));
y=filter(Nz,Dz,x3);
subplot(2,2,1);
plot(n1,x3);
title('������ ������', 'FontName', 'Zapf Chancery');
xlabel('������ �� �������', 'FontName', 'Zapf Chancery');
subplot(2,2,3);
plot(n1,y);
title('������� ������', 'FontName', 'Zapf Chancery');
xlabel('������ �� �������', 'FontName', 'Zapf Chancery');


K=1024;
Px3=fft(x3,K);
px3=abs(Px3(1:K/2));
f=(0:(length(Px3)-1)/2)';
subplot(2,2,2);
plot(f,px3);


Py=fft(y,K);
py=abs(Py(1:K/2));
f1=(0:(length(Py)-1)/2)';
subplot(2,2,4);
plot(f1,py);


mkdir export
cd export
saveas(fig1, 'fig1.png')
saveas(fig2, 'fig2.png')
saveas(fig3, 'fig3.png')
saveas(fig4, 'fig4.png')
cd ..