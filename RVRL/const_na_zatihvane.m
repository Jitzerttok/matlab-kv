function [ const_na_zatihvane ] = const_na_zatihvane( dulgina_na_vulnata, otnostitelna_dielektri4na_pronicaemost, provodimost_na_sredata )
    const_na_zatihvane=2*pi/dulgina_na_vulnata*sqrt(1/2*(-otnostitelna_dielektri4na_pronicaemost+sqrt(otnostitelna_dielektri4na_pronicaemost^2+(60*provodimost_na_sredata*dulgina_na_vulnata)^2)));
end

