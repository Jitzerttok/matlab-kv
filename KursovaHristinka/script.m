clear all
close all
clc

Fs=2610;
Wp=[397 553]/(Fs/2);
Ws=[380 580]/(Fs/2);
Rs=42;
Rp=2;
format long g

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ������ �� ��������
figure(1);
plot([0 230],[42 42],'b');
hold on
plot([230 230],[42 36],'b');
plot([230 330],[36 36],'b');
plot([330 330],[36 6.86],'b');
plot([330 380],[6.86 6.86],'b');
plot([380 380],[6.86 0],'b');

plot([397 397],[2 2.5],'b');
plot([397 553],[2 2],'b');
plot([553 553],[2 2.5],'b');

plot([580 580],[0 6.86],'b');
plot([580 630],[6.86 6.86],'b');
plot([630 630],[6.86 36],'b');
plot([630 730],[36 36],'b');
plot([730 730],[36 42],'b');
plot([730 Fs/2],[42 42],'b');

plot([230 380],[42 42],'r-');
plot([380 380],[6.86 42],'r-');
plot([580 730],[42 42],'r-');
plot([580 580],[6.86 42],'r-');
title('Gabarit'); 
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');


% ������������ �� ������ ��
[N, Wn]=cheb1ord(Wp, Ws, Rp, Rs);
[Nz, Dz]=cheby1(N, Rp, Wn);

% ���������� �� ����������� �� ��
fprintf('���: %d\n', N*2); 
fprintf('Nz:\n');
disp(Nz');
fprintf('Dz:\n');
disp(Dz');
[z,p,~]=tf2zp(Nz, Dz);
fprintf('������:\n');
disp(p);
fprintf('����:\n');
disp(z);

% ����������� �� ��������� � ���
[H, w]=freqz(Nz, Dz, 6969);
filt(Nz,Dz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ������ �� ��������� + ��������
figure(2);
plot(w*Fs/(2*pi), -20*log10(abs(H)));hold on
plot([0 230],[42 42],'b');

plot([230 230],[42 36],'b');
plot([230 330],[36 36],'b');
plot([330 330],[36 6.86],'b');
plot([330 380],[6.86 6.86],'b');
plot([380 380],[6.86 0],'b');

plot([397 397],[2 2.5],'b');
plot([397 553],[2 2],'b');
plot([553 553],[2 2.5],'b');

plot([580 580],[0 6.86],'b');
plot([580 630],[6.86 6.86],'b');
plot([630 630],[6.86 36],'b');
plot([630 730],[36 36],'b');
plot([730 730],[36 42],'b');
plot([730 Fs/2],[42 42],'b');

plot([230 380],[42 42],'r-');
plot([380 380],[6.86 42],'r-');
plot([580 730],[42 42],'r-');
plot([580 580],[6.86 42],'r-');
title('Gabarit'); 
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');axis([0 Fs/2 0 50]);
title('Zatihvane'); 
ylabel('Zatihvane[dB]'); 
xlabel('Freqency[Hz]');
% ���� ������ �� ���������� ������� �� ��

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ������ �� �������-�������� ��������
figure(3);
zplane(Nz,Dz);

% ������ �� ��� ��������
figure(4);
plot(w*Fs/(2*pi), unwrap(angle(H))*180/pi);
title('F4X'); grid;
xlabel('Freqency[Hz]');
ylabel('Phase[deg]'); %����
xlim([0 Fs/2]);

% ������ �� ��
figure(5);
impz(Nz, Dz); grid;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n=(0:99)/Fs;
K=2028;
%Filturut ne propuska
f=600;
signal=sin((2*pi*f).*n);
figure(6);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Magnitude');
xlabel('O4eti');
title('Input Signal f=600Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[~, w]=freqz(Nz,Dz, K/2);
plot(w*Fs/(2*pi), px); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Input Signal Spectrum');
y=filter(Nz, Dz, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Magnitude');
xlabel('O4eti');
title('Output Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Output Signal Spectrum');

%Filturut propuska
f=450;
signal=sin((2*pi*f).*n);
figure(7);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Magnitude');
xlabel('O4eti');
title('Input Signal f=450Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[~, w]=freqz(Nz,Dz, K/2);
plot(w*Fs/(2*pi), px); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Input Signal Spectrum');
y=filter(Nz, Dz, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Magnitude');
xlabel('O4eti');
title('Output Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Output Signal Spectrum');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [Ns,Ds] = ibilinear( Nz, Dz, Fs );
% [H, wz]=freqz(Nz, Dz, 6969);
% [T, ws]=freqs(Ns, Ds, 6969);
% figure(8);
% subplot(211);
% plot(wz*Fs/(2*pi), -20*log10(abs(H)));
% rectangle('Position', [0 0 Ws(1)*(Fs/2) Rs], 'EdgeColor', [1 0 1]);
% rectangle('Position', [Ws(2)*(Fs/2) 0 10000 Rs], 'EdgeColor', [1 0 1]);
% rectangle('Position', [Wp(1)*(Fs/2) Rp (Wp(2)-Wp(1))*(Fs/2) 100000], 'EdgeColor', [1 0 1]);
% axis([0 Fs/2 0 50]);
% title('Zatihvane na Cifrov Filur'); 
% ylabel('Zatihvane[dB]'); 
% xlabel('Freqency[Hz]');
% subplot(212);
% plot(ws/(2*pi), -20*log10(abs(T)));
% rectangle('Position', [0 0 Ws(1)*(Fs/2) Rs], 'EdgeColor', [1 0 1]);
% rectangle('Position', [Ws(2)*(Fs/2) 0 10000 Rs], 'EdgeColor', [1 0 1]);
% rectangle('Position', [Wp(1)*(Fs/2) Rp (Wp(2)-Wp(1))*(Fs/2) 100000], 'EdgeColor', [1 0 1]);
% axis([0 Fs/2 0 50]);
% title('Zatihvane na Analogov Filur'); 
% ylabel('Zatihvane[dB]'); 
% xlabel('Freqency[Hz]');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� X %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N, Wn]=cheb1ord(d2cF(Wp*(Fs/2), Fs), d2cF(Ws*(Fs/2), Fs), Rp, Rs, 's');
[Ns, Ds]=cheby1(N, Rp, Wn, 's');
[H, wz]=freqz(Nz, Dz, 6969);
[T, ws]=freqs(Ns, Ds, 6969);
figure(9);
subplot(211);
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
hold on;
plot([0 230],[42 42],'b');

plot([230 230],[42 36],'b');
plot([230 330],[36 36],'b');
plot([330 330],[36 6.86],'b');
plot([330 380],[6.86 6.86],'b');
plot([380 380],[6.86 0],'b');

plot([397 397],[2 2.5],'b');
plot([397 553],[2 2],'b');
plot([553 553],[2 2.5],'b');

plot([580 580],[0 6.86],'b');
plot([580 630],[6.86 6.86],'b');
plot([630 630],[6.86 36],'b');
plot([630 730],[36 36],'b');
plot([730 730],[36 42],'b');
plot([730 Fs/2],[42 42],'b');

plot([230 380],[42 42],'r-');
plot([380 380],[6.86 42],'r-');
plot([580 730],[42 42],'r-');
plot([580 580],[6.86 42],'r-');
axis([0 Fs/2 0 50]);
title('Zatihvane na Cifrov Filur'); 
ylabel('Zatihvane[dB]'); 
xlabel('Freqency[Hz]');
subplot(212);
plot(ws/(2*pi), -20*log10(abs(T)));
hold on;
plot([0 230],[42 42],'b');

plot([230 230],[42 36],'b');
plot([230 330],[36 36],'b');
plot([330 330],[36 6.86],'b');
plot([330 380],[6.86 6.86],'b');
plot([380 380],[6.86 0],'b');

plot([397 397],[2 2.5],'b');
plot([397 553],[2 2],'b');
plot([553 553],[2 2.5],'b');

plot([580 580],[0 6.86],'b');
plot([580 630],[6.86 6.86],'b');
plot([630 630],[6.86 36],'b');
plot([630 730],[36 36],'b');
plot([730 730],[36 42],'b');
plot([730 Fs/2],[42 42],'b');

plot([230 380],[42 42],'r-');
plot([380 380],[6.86 42],'r-');
plot([580 730],[42 42],'r-');
plot([580 580],[6.86 42],'r-');
axis([0 Fs/2 0 50]);
title('Zatihvane na Analogov Filur'); 
ylabel('Zatihvane[dB]'); 
xlabel('Freqency[Hz]');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%