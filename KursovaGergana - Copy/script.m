clear all
close all
clc
format shortEng
format compact

Fs=4570;
Wp=[1310 1362];
Ws=[1222 1464];
Rs = 25;
Rp = 0.1;
K=2048;
i=1;
color = [0.7250 0.7250 0.7250];
granici=[0 Fs/2 0 30];

%����� 1
%������ �������
figure(i); i=i+1;
rectangle('Position', [0 0 Ws(1) Rs], 'FaceColor', color); % ����� ����� �� ��������� 
rectangle('Position', [Wp(1) Rp (Wp(2)-Wp(1)) 30], 'FaceColor', color); % ����� �� ���������� 
rectangle('Position', [Ws(2) 0 10000 Rs], 'FaceColor', color); % ����� ����� �� ��������� 
axis(granici);
title('Gabarit na analogov LF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%������ ������� (���������)
figure(i); i=i+1;
rectangle('Position', [0 0 Ws(1) Rs], 'FaceColor', color); % ����� ����� �� ��������� 
rectangle('Position', [Wp(1) Rp (Wp(2)-Wp(1)) 30], 'FaceColor', color); % ����� �� ���������� 
rectangle('Position', [Ws(2) 0 10000 Rs], 'FaceColor', color); % ����� ����� �� ���������  
axis([1000 1800 0 1]);
title('Gabarit na analogov LF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%����� 2
%������������ �� ��������� 
%������������ �� �������� ��
[N, Wn] = buttord(Wp*(2*pi), Ws*(2*pi), Rp, Rs, 's');
[Ns, Ds] = butter(N,  Wn, 's');
[T, ws]=freqs(Ns, Ds, K);

fprintf('������ ������: \n'); 
fprintf('���: %d\n', N*2); 
fprintf('Ns:\n');
disp(Ns');
fprintf('Ds:\n');
disp(Ds')

%����� 3
[Nz, Dz]=bilinear(Ns, Ds, Fs);
[H, wz]=freqz(Nz, Dz, K);
fprintf('Nz:\n');
disp(Nz');
fprintf('Dz:\n');
disp(Dz')

%����� 3
%���������
figure(i); i=i+1;
subplot(211);
hold on;
plot(ws/(2*pi), -20*log10(abs(T)));
rectangle('Position', [0 0 Ws(1) Rs], 'FaceColor', color); % ����� ����� �� ��������� 
rectangle('Position', [Wp(1) Rp (Wp(2)-Wp(1)) 30], 'FaceColor', color); % ����� �� ���������� 
rectangle('Position', [Ws(2) 0 10000 Rs], 'FaceColor', color); % ����� ����� �� ��������� 
axis(granici);
title('Zatihvane na analogov LF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');
subplot(212);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
rectangle('Position', [0 0 ad(Ws(1), Fs) Rs], 'FaceColor', color); % ����� ����� �� ��������� 
rectangle('Position', [ad(Wp(1), Fs) Rp ad((Wp(2)-Wp(1)), Fs) 30], 'FaceColor', color); % ����� �� ���������� 
rectangle('Position', [ad(Ws(2), Fs) 0 10000 Rs], 'FaceColor', color); % ����� ����� �� ��������� 
axis(granici);
title('Zatihvane na cifrov LF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

figure(i); i=i+1;
subplot(211);
hold on;
plot(ws/(2*pi), -20*log10(abs(T)));
rectangle('Position', [0 0 Ws(1) Rs], 'FaceColor', color); % ����� ����� �� ��������� 
rectangle('Position', [Wp(1) Rp (Wp(2)-Wp(1)) 30], 'FaceColor', color); % ����� �� ���������� 
rectangle('Position', [Ws(2) 0 10000 Rs], 'FaceColor', color); % ����� ����� �� ��������� 
axis([1000 1800 0 1]);
title('Zatihvane na analogov LF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');
subplot(212);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
rectangle('Position', [0 0 ad(Ws(1), Fs) Rs], 'FaceColor', color); % ����� ����� �� ��������� 
rectangle('Position', [ad(Wp(1), Fs) Rp ad((Wp(2)-Wp(1)), Fs) 30], 'FaceColor', color); % ����� �� ���������� 
rectangle('Position', [ad(Ws(2), Fs) 0 10000 Rs], 'FaceColor', color); % ����� ����� �� ��������� 
axis([1000 1800 0 1]);
title('Zatihvane na cifrov LF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%����� 4
% ���
figure(i); i=i+1;
pzmap(Ns, Ds);
title('PND na analogov LF');
figure(i); i=i+1;
zplane(Nz, Dz);
title('PND na cifrov LF');
figure(i); i=i+1;
zplane(Nz, Dz);
axis([0.96 1.06 -0.04 0.04]);
title('PND na cifrov LF');

[z,p,~]=tf2zpk(Ns, Ds);
fprintf('�������� ������:\n');
fprintf('����:\n');
disp(z);
fprintf('������:\n');
disp(p);
[z,p,~]=tf2zpk(Nz, Dz);
fprintf('������ ������:\n');
fprintf('����:\n');
disp(z);
fprintf('������:\n');
disp(p);


% ���
figure(i); i=i+1;
subplot(211);
hold on;
plot(wz*Fs/(2*pi), unwrap(angle(H))*180/pi);
grid;
title('F4X na cifrov LF');
ylabel('Faza[deg]');
xlabel('Freqency[Hz]');
xlim([0 Fs/2]);
subplot(212);
hold on;
plot(ws/(2*pi), unwrap(angle(T))*180/pi);
grid;
xlim([0 Fs/2]);
title('F4X na analogov LF');
ylabel('Faza[deg]');
xlabel('Freqency[Hz]');

% ��
figure(i); i=i+1;
impz(Nz, Dz); grid;
title('IX na cifrov LF');
figure(i); i=i+1;
[imp,~,t]=impulse(Ns, Ds);
plot(t, imp);
xlabel('Vreme');
ylabel('Amplituda');
title('IX za analogov filur'); grid;

n=(0:99)/Fs;

F=1080;
x = sin(2*pi*F*n);
figure(i); i=i+1;
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden signal f=1080Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,3);plot(n,y);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden signal');
 
X=fft(x,K*2);
subplot(2,2,2);plot(wz*Fs/(2*pi),abs(X(1:K))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Vhoden signal amplituden spektur');
 
Y=fft(y,K*2);
subplot(2,2,4);plot(wz*Fs/(2*pi),abs(Y(1:K))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Izhoden signal amplituden spektur');

F=1600;
x = sin(2*pi*F*n);
figure(i); i=i+1;
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden signal f=1600Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,3);plot(n,y);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden signal');
 
X=fft(x,K*2);
subplot(2,2,2);plot(wz*Fs/(2*pi),abs(X(1:K))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Vhoden signal amplituden spektur');
 
Y=fft(y,K*2);
subplot(2,2,4);plot(wz*Fs/(2*pi),abs(Y(1:K))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Izhoden signal amplituden spektur');