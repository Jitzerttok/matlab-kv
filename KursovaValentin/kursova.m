close all;
clc;

Wp=[1447 1507]; %Hz
Ws=[1349 1620]; %Hz
Rs=25; %dB
Rp=0.1; %dB
Fs=4.41*10^3; %Hz
K=4096;

% ������� �� ���
figure(1);
hold on;
plot([0 Ws(1)], [Rs Rs],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(2) 10000], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
plot([Wp(1) Wp(2)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+0.5],'r');
plot([Wp(2) Wp(2)], [Rp Rp+0.5],'r');
axis([0 Fs/2 0 Rs+5]);
title('Gabarit na cifrov lenov filtur');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');
figure(2);
hold on;
plot([0 Ws(1)], [Rs Rs],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(2) 10000], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
plot([Wp(1) Wp(2)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+0.2],'r');
plot([Wp(2) Wp(2)], [Rp Rp+0.2],'r');
axis([1300 1700 0 18*Rp]);
title('Gabarit na cifrov lenov filtur (lenta na propuskane)');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');

% ������������ �� ���
[N,Wn]=cheb1ord(Wp/(Fs/2), Ws/(Fs/2), Rp, Rs);
[Nz, Dz]=cheby1(N, Rp, Wn);
fprintf('���: %d', N*2);
filt(Nz, Dz)
[H, wz]=freqz(Nz, Dz, K);
wz=wz*Fs;

% ��������� + ������� �� ���
figure(3);
hold on;
plot([0 Ws(1)], [Rs Rs],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(2) 10000], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
plot([Wp(1) Wp(2)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+0.5],'r');
plot([Wp(2) Wp(2)], [Rp Rp+0.5],'r');
plot(wz/(2*pi), -20*log10(abs(H)));
axis([0 Fs/2 0 Rs+5]);
title('Zatihvane + Gabarit na cifrov lenov filtur');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');
figure(4);
hold on;
plot([0 Ws(1)], [Rs Rs],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(2) 10000], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
plot([Wp(1) Wp(2)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+0.2],'r');
plot([Wp(2) Wp(2)], [Rp Rp+0.2],'r');
plot(wz/(2*pi), -20*log10(abs(H)));
axis([1300 1700 0 18*Rp]);
title('Zatihvane + Gabarit na cifrov lenov filtur (lenta na propuskane)');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');

% ��� ��� � �� �� ���
figure(5);
zplane(Nz,Dz);
title('Polusno Nuleva Diagrama');
figure(6);
zplane(Nz,Dz);
axis([1-10^-5 1+10^-5 -5*10^-6 5*10^-6]);
title('Polusno Nuleva Diagrama ( Nulite v (1;0) )');
[z,p,~]=tf2zp(Nz, Dz);
fprintf('������:\n');
disp(p);
fprintf('����:\n');
disp(z);
figure(7);
plot(wz/(2*pi), unwrap(angle(H))*180/pi);
xlabel('Frequency[Hz]');
ylabel('Faza[deg]');
xlim([0 Fs/2]);
figure(8);
impz(Nz,Dz);
title('Impulsna Harakteristika');

% ����������� �� �������� ����������
[b0, B, A]=dir2cas(Nz, Dz);
fprintf('����������� �� �������� ����������:\n');
fprintf('b0:\n');
disp(b0);
fprintf('B:\n');
disp(B);
fprintf('A:\n');
disp(A);

% ����� ������
n=(0:99)/Fs;
K=1024;
%Filturut ne propuska
f=900;
signal=sin((2*pi*f).*n);
figure(9);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Amplituda');
xlabel('O4eti');
title('Vhoden Signal f=900Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[~, w]=freqz(Nz,Dz, K/2);
plot(w*Fs/(2*pi), px); grid;
axis([0 Fs 0 50]);
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Vhoden Signal Spectur');
y=filter(Nz, Dz, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Amplituda');
xlabel('O4eti');
title('Izhoden Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
axis([0 Fs 0 60]);
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Izhoden Signal Spectur');

%Filturut propuska
f=1470;
signal=sin((2*pi*f).*n);
figure(10);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Amplituda');
xlabel('O4eti');
title('Vhoden Signal f=1470Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[~, w]=freqz(Nz,Dz, K/2);
plot(w*Fs/(2*pi), px); grid;
axis([0 Fs 0 50]);
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Vhoden Signal Spectur');
y=filter(Nz, Dz, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Amplituda');
xlabel('O4eti');
title('Izhoden Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
axis([0 Fs 0 60]);
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Izhoden Signal Spectur');

% ������������ �� ���
[N, Wn]=cheb1ord(ad(Wp, Fs)*(2*pi), ad(Ws, Fs)*(2*pi), Rp, Rs,'s');
[Ns, Ds]=cheby1(N, Rp, Wn,'s');
[T, ws]=freqs(Ns, Ds, K);
fprintf('���: %d', N*2);
printsys(Ns, Ds);

% ��������� + ������� �� ��� � ���
figure(11);
subplot(211);
hold on;
plot([0 Ws(1)], [Rs Rs],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(2) 10000], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
plot([Wp(1) Wp(2)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+0.5],'r');
plot([Wp(2) Wp(2)], [Rp Rp+0.5],'r');
plot(wz/(2*pi), -20*log10(abs(H)));
axis([0 Fs/2 0 Rs+5]);
title('Zatihvane + Gabarit na cifrov lenov filtur');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');
subplot(212);
hold on;
plot([0 ad(Ws(1), Fs)], [Rs Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(2), Fs) 10000], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');
plot([ad(Wp(1), Fs) ad(Wp(2), Fs)], [Rp Rp],'r');
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+0.5],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+0.5],'r');
plot(ws/(2*pi), -20*log10(abs(T)));
axis([0 Fs/2 0 Rs+5]);
title('Zatihvane + Gabarit na analogov lenov filtur');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');

figure(12);
subplot(211);
hold on;
plot([0 Ws(1)], [Rs Rs],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(2) 10000], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
plot([Wp(1) Wp(2)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+0.5],'r');
plot([Wp(2) Wp(2)], [Rp Rp+0.5],'r');
plot(wz/(2*pi), -20*log10(abs(H)));
axis([1300 1700 0 18*Rp]);
title('Zatihvane + Gabarit na cifrov lenov filtur (lenta na propuskane)');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');
subplot(212);
hold on;
plot([0 ad(Ws(1), Fs)], [Rs Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(2), Fs) 10000], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');
plot([ad(Wp(1), Fs) ad(Wp(2), Fs)], [Rp Rp],'r');
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+0.5],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+0.5],'r');
plot(ws/(2*pi), -20*log10(abs(T)));
axis([1000 1280 0 18*Rp]);
title('Zatihvane + Gabarit na analogov lenov filtur (lenta na propuskane)');
xlabel('Frequency[Hz]');
ylabel('Zatihvane[dB]');