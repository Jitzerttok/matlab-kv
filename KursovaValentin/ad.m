function [ w ] = ad( W, Fs )
    T=1/Fs;
    w=2/T*atan(T*W*pi)/(2*pi);
end

