N = [3, 5, 7, 12];
style = [ 'y-', 'm--', 'r:', 'k-.'];
Rp = 3;
Rs = 25;

for i=1:length(N)
    [z, p ,k] = ellipap(N(i), Rp, Rs);
    [Ns, Ds] = zp2tf(z,p,k);
    [T, w] = freqs(Ns, Ds);
    a = -20*log10(abs(T));
    plot(w,a, style(i));
    hold on;
end
legend('N=3','N=5','N=7', 'N=12')
grid;
