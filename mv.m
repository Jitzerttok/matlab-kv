close all
clear all
clc
Ns = [0 0 0 0 0 0.03];
Ds = [1 1.62 1.31 0.65 0.21 0.03];

[T,w]=freqs(Ns,Ds);
m=abs(T);
fi=angle(T);
fi1=unwrap(fi);

fig1 = figure(1);
printsys(Ns,Ds,'s')

pzmap(Ns,Ds)

fig2 = figure(2);
subplot(211),plot(w,m);
ylabel('���������','FontName', 'ZapfChancery');
xlabel('������� (rad/s)','FontName', 'ZapfChancery');
title('����������-�������� ��������������','FontName', 'ZapfChancery');
set(gca, 'FontName', 'ZapfChancery'); grid;

subplot(212);
plot(w,fi1*180/pi);
ylabel('���','FontName', 'ZapfChancery');
xlabel('������� (rad/s)','FontName', 'ZapfChancery');
title('������-�������� ��������������','FontName', 'ZapfChancery'); grid;

fig3 = figure(3);
subplot(211);
loglog(w,m);
ylabel('���������','FontName', 'ZapfChancery');
xlabel('������� (rad/s)','FontName', 'ZapfChancery');
title('����������-�������� ��������������','FontName', 'ZapfChancery'); grid;

subplot(212);
semilogx(w,fi1*180/pi);
ylabel('��� (�������)','FontName', 'ZapfChancery');
xlabel('������� (rad/s)','FontName', 'ZapfChancery');
title('������-�������� ��������������','FontName', 'ZapfChancery'); grid;

fig4 = figure(4);
m1=20*log10(m);

subplot(211);
plot(w,m1);
ylabel('��������� � dB','FontName', 'ZapfChancery');
xlabel('������� (rad/s)','FontName', 'ZapfChancery');
title('����������-�������� ��������������','FontName', 'ZapfChancery'); grid;

subplot(212);
plot(w,fi1*180/pi);
ylabel('���','FontName', 'ZapfChancery');
xlabel('������� (rad/s)','FontName', 'ZapfChancery');
title('������-�������� ��������������','FontName', 'ZapfChancery');
grid;

fig5 = figure(5);
[imp,x,t]=impulse(Ns,Ds);
subplot(211);
plot(t,imp);

ylabel('��������� (volts)','FontName', 'ZapfChancery');
xlabel('����� (s)','FontName', 'ZapfChancery');
title('�������� ��������������','FontName', 'ZapfChancery');
grid;

[st,x,t]=step(Ns,Ds);
subplot(212);
plot(t,st);
ylabel('��������� (volts)','FontName', 'ZapfChancery');
xlabel('����� (s)','FontName', 'ZapfChancery');
title('�������� ��������������','FontName', 'ZapfChancery');
grid;

mkdir exports
saveas(fig1,'exports/fig1.png');
saveas(fig2,'exports/fig2.png');
saveas(fig3,'exports/fig3.png');
saveas(fig4,'exports/fig4.png');
saveas(fig5,'exports/fig5.png');


