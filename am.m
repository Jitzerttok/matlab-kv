syms t
A0=1;
f0=10^4;
Am=0.5;
fm=100;

sm = Am*cos(2*pi*fm*t);
sam = (A0+sm)*cos(2*pi*f0*t);

figure(1);
ezplot(sam, [100*-1/f0 100*1/f0]);

N=5;
k=-2;
spectrum = zeros(1, N);
freq = zeros(1, N);
for i=1:N
    spectrum(i) = int(sam*exp(-1i * 2*pi*(f0+k*fm)*t), t, 0, 1/f0);
    freq(i) = f0+k*fm;
    k=k+1;
end

stem(freq, abs(spectrum));