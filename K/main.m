close all;
clear all;
clc;

Wp=[500 1190];
Ws=[660 990];
Rp=0.9;
Rs=40;
Fs=4630;
K=4096;

% ������� �� ������ ��
figure(1);
hold on;
axis([0 Fs/2 0 Rs+5]);
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
title('Utejnen gabarit cifrov na RF');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');

% ������������ �� ������ ��
[N, Wn]=ellipord(Wp/(Fs/2), Ws/(Fs/2), Rp, Rs);
[Nz, Dz]=ellip(N, Rp, Rs, Wn, 'stop');
fprintf('��� �� �������: %d\n', N*2);
filt(Nz, Dz)

% ������ �� ��������� �� ������ �� + �������
[H, wz] = freqz(Nz, Dz, K);

figure(2);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
axis([0 Fs/2 0 Rs+20]);
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
title('Utejnen gabarit cifrov na RF + zatihvane');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');


%������ �� ���
figure(3);
zplane(Nz,Dz);
[z, p, ~]=tf2zpk(Nz, Dz);
fprintf('������:\n');
disp(p);
fprintf('����:\n');
disp(z);

%������ �� ���
figure(4);
plot(wz*Fs/(2*pi), unwrap(angle(H))*180/pi);
title('F4X');
xlabel('4estota[Hz]');
ylabel('Faza[deg]'); %����
xlim([0 Fs/2]);

% ������ �� ��
figure(5);
impz(Nz, Dz);

% ����������� �� �������� ����������
[b0, B, A]=dir2cas(Nz, Dz);
fprintf('b0=\n');
disp(b0);
fprintf('B=\n');
disp(B);
fprintf('A=\n');
disp(A);

% �������� �� �������
f=900;
n=(0:99)/Fs;
x = sin(2*pi*f*n);
figure(6);
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda[dB]');
title('Vhoden vuv vremeto signal f=900Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,2);plot(n,y);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden vuv vremeto signal');
 
Px=fft(x,K*2);
px=abs(Px(1:K));
subplot(2,2,3);plot(wz*Fs/(2*pi),px); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na vhoden signal');
 
Py=fft(y,K*2);
py=abs(Py(1:K));
subplot(2,2,4);plot(wz*Fs/(2*pi),py); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na izhoden signal');

f=1500;
n=(0:99)/Fs;
x = sin(2*pi*f*n);
figure(7);
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda[dB]');
title('Vhoden vuv vremeto signal f=1500Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,2);plot(n,y);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden vuv vremeto signal');
 
Px=fft(x,K*2);
px=abs(Px(1:K));
subplot(2,2,3);plot(wz*Fs/(2*pi),px); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na vhoden signal');
 
Py=fft(y,K*2);
py=abs(Py(1:K));
subplot(2,2,4);plot(wz*Fs/(2*pi),py); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na izhoden signal');

% �������������� �� ������ �� ��� �������� ��
[N, Wn]=ellipord(d2cF(Wp, Fs), d2cF(Ws, Fs), Rp, Rs, 's');
[Ns, Ds]=ellip(N, Rp, Rs, Wn, 'stop', 's');

[T, ws] = freqs(Ns, Ds, K);

figure(8);
hold on;
plot(ws/(2*pi), -20*log10(abs(T)));
axis([0 Fs/2 0 Rs+20]);
plot([0 Wp(1)], [Rp Rp],'r');
plot([d2cF(Wp(1), Fs)/(2*pi) d2cF(Wp(1), Fs)/(2*pi)], [Rp Rp+2],'r');
plot([d2cF(Wp(2), Fs)/(2*pi) 10000], [Rp Rp],'r');
plot([d2cF(Wp(2), Fs)/(2*pi) d2cF(Wp(2), Fs)/(2*pi)], [Rp Rp+2],'r');
plot([d2cF(Ws(1), Fs)/(2*pi) d2cF(Ws(1), Fs)/(2*pi)], [0 Rs],'r');
plot([d2cF(Ws(1), Fs)/(2*pi) d2cF(Ws(2), Fs)/(2*pi)], [Rs Rs],'r');
plot([d2cF(Ws(2), Fs)/(2*pi) d2cF(Ws(2), Fs)/(2*pi)], [0 Rs],'r');
title('Utejnen gabarit analogov na RF + zatihvane');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');
filt(Nz, Dz)
printsys(Ns, Ds);