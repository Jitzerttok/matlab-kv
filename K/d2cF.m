function [ w ] = d2cF( W, Fs )
	T=1/Fs;
    W=W*(2*pi);
    w=2/T*atan(W*T/2);
end

