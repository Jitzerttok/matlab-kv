function F = FE( tetha,LE,lambda )
    tri = pi*LE*sin(deg2rad(tetha))/lambda;
    secx = @(t) sin(t)./t;
    F=secx(tri);
end

