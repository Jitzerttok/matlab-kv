% ������� �� ����� ������������� �� �����
function [spectral, frequency] = digital_fourie_calculator(signal, period, num_fourier_row)
[~, num_discretes] = size(signal);
spectral = zeros(1,num_fourier_row);
frequency = zeros(1,num_fourier_row);
for k = 1:num_fourier_row,
    for n=1:num_discretes,
        spectral(k) = spectral(k) + signal(n) * exp(-1i * 2 * pi * (k - 1) * (n - 1) /num_discretes);
    end
    spectral(k) = spectral(k) / num_discretes;
    frequency(k) = (k - 1) * 2 * pi/period;
end
end