function [ spectrum, freaqencies ] = fourierCalculator( signal, period, nRows )
syms t;
spectrum = zeros(1, nRows+1);
freaqencies = zeros(1, nRows+1);
for k=1:nRows+1
    spectrum(k) = 2*int(signal*exp(-1i * 2*pi/period * (k-1) * t), t, 0, period)/period;
    freaqencies(k) = 2*pi/period*(k-1);
end
    spectrum(1) = spectrum(1)/2;
end

