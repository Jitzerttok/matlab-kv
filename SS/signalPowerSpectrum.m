function [ power, p ] = signalPowerSpectrum( spectrum)
n=length(spectrum);
sum = 0;
p=zeros(1,n);
for i=2:n
    p(i) = abs(spectrum(i))^2/2;
    sum = sum + p(i);
end
p(1) = abs(spectrum(1))^2;
power = p(1) + sum;
end

