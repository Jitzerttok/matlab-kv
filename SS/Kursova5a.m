close all
clear all

syms t;

nRows = 5;
amplitude = 9;
period = 6*10^-3;

signal = amplitude*sin(2*pi/period*t)*(1-heaviside(t-period/2));

fig1 = figure(1);
ezplot(signal, [0 period]);
xlabel('Time[s]');
ylabel('Magnitude[V]');
title('Signal');
grid;

[spectrum, freaqencies] = fourierCalculator( signal, period, nRows+1 );

if abs(spectrum(1))~=0
    fprintf('��������� �������� ���������[V]: %f ����[�������]: %f\n',abs(spectrum(1)),angle(spectrum(1))*180/pi);
else 
    fprintf('��������� �������� ����\n');
end
for i=1:nRows+1
    if abs(spectrum(i+1))~=0
        fprintf('�������� #%d ���������[V]: %f ����[�������]: %f\n',i,abs(spectrum(i+1)),angle(spectrum(i+1))*180/pi);
    end
end
fprintf('\n');

fig2 = figure(2);
subplot(211)
stem(freaqencies/(2*pi), abs(spectrum));
xlabel('Freaqency[Hz]');
ylabel('Magnitude[V]');
title('AFSD');
grid;
subplot(212)
stem(freaqencies/(2*pi), angle(spectrum)*180/pi);
xlabel('Freaqency[Hz]');
ylabel('Phase[deg]');
title('PFSD');
grid;

avgPower = signalPower( signal, period );
fprintf('������ ������� �� �������: %f W\n', avgPower);

[sctPowerAvg, sctPower] = signalPowerSpectrum(spectrum);

if abs(sctPower(1))~=0
    fprintf('��������� �������� �������: %f W\n', sctPower(1));
else 
    fprintf('��������� �������� ����\n');
end
for i=1:nRows+1
    if sctPower(i+1)~=0
        fprintf('�������� #%d �������: %f W\n',i, sctPower(i+1));
    end
end
persent = sctPowerAvg/avgPower;
fprintf('Average from signal: %f%%\n', persent*100);