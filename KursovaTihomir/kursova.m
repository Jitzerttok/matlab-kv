close all;
clear all;
clc;

Wp=[620 820];
Ws=[637 793];
Rp=2;
Rs=42;
Fs=2660;
K=1024*4;
format long g

%����� 1
figure(1);
hold on;
title('Gabarit za analogov filtur');
xlabel('Freqency[Hz]');
ylabel('Zatihvane');
axis([0 Fs/2 0 60]);
plot([0 Wp(1)], [Rp Rp],'r'); % r - ������ ����
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');

figure(2);
hold on;
title('Gabarit za cifrov filtur');
xlabel('Freqency[Hz]');
ylabel('Zatihvane');
axis([0 Fs/2 0 60]);
plot([0 ad(Wp(1), Fs)], [Rp Rp],'r'); % r - ������ ����
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+2],'r');
plot([ad(Wp(2), Fs) 10000], [Rp Rp],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+2],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(2), Fs)], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');

%����� 2
[N, Wn]=cheb1ord(Wp*(2*pi), Ws*(2*pi), Rp, Rs, 's');
[Ns, Ds]=cheby1(N, Rp, Wn, 'stop', 's');
[Nz, Dz]=bilinear(Ns, Ds, Fs);
[T, ws]=freqs(Ns, Ds, K/2);
[H, wz]=freqz(Nz, Dz, K/2);
fprintf('�������� ������:\n');
printsys(Ns, Ds);
fprintf('������ ������:\n');
filt(Nz, Dz)

%����� 3
figure(3);
subplot(211);
hold on;
plot(ws/(2*pi), -20*log10(abs(T)));
title('Gabarit za analogov filtur');
xlabel('Freqency[Hz]');
ylabel('Zatihvane');
axis([0 Fs/2 0 60]);
plot([0 Wp(1)], [Rp Rp],'r'); % r - ������ ����
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
subplot(212);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
title('Gabarit za cifrov filtur');
xlabel('Freqency[Hz]');
ylabel('Zatihvane');
axis([0 Fs/2 0 60]);
plot([0 ad(Wp(1), Fs)], [Rp Rp],'r'); % r - ������ ����
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+2],'r');
plot([ad(Wp(2), Fs) 10000], [Rp Rp],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+2],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(2), Fs)], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');

%����� 4
figure(4);
pzmap(Ns, Ds);
figure(5);
pzmap(Ns, Ds);
axis([-400 300 2500 6000]);
title('PND za analogov filur');
[z,p,~]=tf2zpk(Ns, Ds);
fprintf('�������� ������:\n');
fprintf('����:\n');
disp(z);
fprintf('������:\n');
disp(p);
figure(6);
plot(ws/(2*pi), unwrap(angle(T))*180/pi);
title('F4H za analogov filtur');
xlabel('Freqency[Hz]');
ylabel('Phase[deg]'); grid;
xlim([0 2500]);
figure(7);
[imp,~,t]=impulse(Ns, Ds);
plot(t, imp);
xlabel('Vreme');
ylabel('Amplituda');
title('IX za analogov filur'); grid;

figure(8);
zplane(Nz, Dz);
figure(9);
zplane(Nz, Dz);
axis([-0.2 0.5 0.6 1.2]);
title('PND za cifrov filur');
[z,p,~]=tf2zpk(Nz, Dz);
fprintf('������ ������:\n');
fprintf('����:\n');
disp(z);
fprintf('������:\n');
disp(p);
figure(10);
plot(wz*Fs/(2*pi), unwrap(angle(H))*180/pi);
title('F4H za cifrov filtur');
xlabel('Freqency[Hz]');
ylabel('Phase[deg]'); grid;
xlim([0 Fs/2]);
figure(11);
impz(Nz, Dz);
title('IX za cifrov filur'); grid;

% ����� 5
[C, B, A]=dir2par(Nz, Dz);
fprintf('C=\n');
disp(C);
fprintf('B=\n');
disp(B);
fprintf('A=\n');
disp(A);

n=(0:99)/Fs;

F=1080;
x = sin(2*pi*F*n);
figure(12);
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden signal f=1080Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,3);plot(n,y);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden signal');
 
X=fft(x,K);
subplot(2,2,2);plot(wz*Fs/(2*pi),abs(X(1:K/2))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Vhoden signal amplituden spektur');
 
Y=fft(y,K);
subplot(2,2,4);plot(wz*Fs/(2*pi),abs(Y(1:K/2))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Izhoden signal amplituden spektur');

F=1600;
x = sin(2*pi*F*n);
figure(13);
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden signal f=1600Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,3);plot(n,y);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden signal');
 
X=fft(x,K);
subplot(2,2,2);plot(wz*Fs/(2*pi),abs(X(1:K/2))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Vhoden signal amplituden spektur');
 
Y=fft(y,K);
subplot(2,2,4);plot(wz*Fs/(2*pi),abs(Y(1:K/2))); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Izhoden signal amplituden spektur');