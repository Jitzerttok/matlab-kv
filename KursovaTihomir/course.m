clear all;
close all;
clc;

Wp=[380 580];
Ws=[397 553];

Rp=2;
Rs=42;
Fs=2600;
K=1024*4;

format long g

%����� 1
figure(1);
hold on;
title('�������� �������');
xlabel('�������[Hz]');
ylabel('���������');
axis([0 Fs/2 0 60]);
plot([0 Wp(1)], [Rp Rp],'r'); % r - ������ ����
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');

figure(2);
hold on;
title('������ �������');
xlabel('�������[Hz]');
ylabel('���������');
axis([0 Fs/2 0 60]);
plot([0 ad(Wp(1), Fs)], [Rp Rp],'r'); % r - ������ ����
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+2],'r');
plot([ad(Wp(2), Fs) 10000], [Rp Rp],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+2],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(2), Fs)], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');

%����� 2
[N, Wn]=cheb2ord(Wp, Ws, Rp, Rs, 's');
[Ns, Ds]=cheby2(N, Rs, Wn, 'stop', 's');

[N, Wn]=cheb2ord(ad(Wp,Fs)/(Fs/2), ad(Ws,Fs)/(Fs/2), Rp, Rs);
[Nz, Dz]=cheby2(N, Rs, Wn, 'stop');


[T, ws]=freqs(Ns, Ds, K/2);
[H, wz]=freqz(Nz, Dz, K/2);
fprintf('�������� ������:\n');
printsys(Ns, Ds);
fprintf('������ ������:\n');
filt(Nz, Dz)


%����� 3
figure(3);
subplot(211);
hold on;
plot(ws, -20*log10(abs(T)));
title('�������� �������');
xlabel('�������[Hz]');
ylabel('���������');
axis([0 Fs/2 0 60]);
plot([0 Wp(1)], [Rp Rp],'r'); % r - ������ ����
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
subplot(212);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
title('������ �������');
xlabel('�������[Hz]');
ylabel('���������');
axis([0 Fs/2 0 60]);
plot([0 ad(Wp(1), Fs)], [Rp Rp],'r'); % r - ������ ����
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+2],'r');
plot([ad(Wp(2), Fs) 10000], [Rp Rp],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+2],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(2), Fs)], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');

%����� 4
figure(4);
pzmap(Ns, Ds);
title('��� �� �������� �-�');
[z,p,~]=tf2zpk(Ns, Ds);
fprintf('�������� ������:\n');
fprintf('����:\n');
disp(z);
fprintf('������:\n');
disp(p);
figure(5);
plot(ws, unwrap(angle(T))*180/pi);
title('��� �� �������� ������');
xlabel('�������[Hz]');
ylabel('����[deg]'); grid;
xlim([0 1000]);
figure(6);
[imp,~,t]=impulse(Ns, Ds);
plot(t, imp);
xlabel('�����');
ylabel('���������');
title('�� �� �������� ������'); grid;

figure(7);
zplane(Nz, Dz);
title('��� �� ������ �-�');
[z,p,~]=tf2zpk(Nz, Dz);
fprintf('������ ������:\n');
fprintf('����:\n');
disp(z);
fprintf('������:\n');
disp(p);
figure(8);
plot(wz*Fs/(2*pi), unwrap(angle(H))*180/pi);
title('��� �� ������ �-�');
xlabel('�������[Hz]');
ylabel('����[deg]'); grid;
xlim([0 Fs/2]);
figure(9);
impz(Nz, Dz);
title('�� �� ������ �-�'); grid;

% ����� 5
[C, B, A]=dir2par(Nz, Dz);
fprintf('C=\n');
disp(C);
fprintf('B=\n');
disp(B);
fprintf('A=\n');
disp(A);

n=(0:99)/Fs;

F=400;
x = sin(2*pi*F*n);
figure(10);
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('������ �� ������� n');
ylabel('��������� � dB');
title('������ �-� f=400Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,3);plot(n,y);grid;
ylim([-1 1]);
xlabel('������ �� ������� n');
ylabel('��������� � dB');
title('������� �-�');
 
X=fft(x,K);
subplot(2,2,2);plot(wz*Fs/(2*pi),abs(X(1:K/2))); grid;
ylim([0 60]);
xlabel('������ �� ������� � Hz');
ylabel('��������� � dB');
title('������ �-� ��');
 
Y=fft(y,K);
subplot(2,2,4);plot(wz*Fs/(2*pi),abs(Y(1:K/2))); grid;
ylim([0 60]);
xlabel('������ �� ������� � Hz');
ylabel('��������� � dB');
title('������� �-� ��');

F=600;
x = sin(2*pi*F*n);
figure(11);
subplot(2,2,1);plot(n,x);grid;
ylim([-1 1]);
xlabel('������ �� ������� n');
ylabel('��������� � dB');
title('������ �-� f=600Hz');
 
y=filter(Nz,Dz,x);
subplot(2,2,3);plot(n,y);grid;
ylim([-1 1]);
xlabel('������ �� ������� n');
ylabel('��������� � dB');
title('������� �-�');
 
X=fft(x,K);
subplot(2,2,2);plot(wz*Fs/(2*pi),abs(X(1:K/2))); grid;
ylim([0 60]);
xlabel('������ �� ������� � Hz');
ylabel('��������� � dB');
title('������ �-� ��');
 
Y=fft(y,K);
subplot(2,2,4);plot(wz*Fs/(2*pi),abs(Y(1:K/2))); grid;
ylim([0 60]);
xlabel('������ �� ������� � Hz');
ylabel('��������� � dB');
title('������� �-� ��');