N = 4;
Rs = 40;
Rp = 3;

[z, p ,k] = cheb2ap(N, Rs);
[Ns, Ds] = zp2tf(z,p,k);

figure(1);
[T, w] = freqs(Ns, Ds);

subplot(2,2,[1 3]);
pzmap(Ns, Ds);

subplot(2,2,2);
plot(w, 20*log10(abs(T))); grid;

subplot(2,2,4);
plot(w, angle(T)); grid;

Wo = 800;
Bw = 200;
[Ns, Ds] = lp2bp(Ns, Ds, Wo, Bw);

figure(2);
[T, w] = freqs(Ns, Ds);

subplot(2,2,[1 3]);
pzmap(Ns, Ds);
xlim([-60 0]);

subplot(2,2,2);
plot(w, 20*log10(abs(T))); grid;
xlim([0 1000]);

subplot(2,2,4);
plot(w, angle(T)); grid;
xlim([0 1000]);