


Nz = []
Dz = []

def gen(lst):
    eq = ""
    for i in range(0, len(lst)):
        sign = "+"
        number = lst[i]
        n = ("%.2g" % (number,))
        z = "z^-"+str(i)

        if number < 0:
            sign = ""

        if number == -1:
            sign = "-"
        
        if number == 0:
            continue

        if i == 0:
            sign = ""
            z = ""

        if i!=0 and (number == 1 or number == -1):
            n = ""

        eq = eq + (sign + n + z)
    
    return eq

with open('input.txt', 'r') as fp:
    lineType = None
    for cnt, line in enumerate(fp):
       if not line.strip() or line in ['\n', '\r\n']:
           continue

       if 'Nz' in line:
            lineType = 'Nz'
            continue
            
       if 'Dz' in line:
            lineType = 'Dz'
            continue

       n = None
       try:
           n = float(line)
       except ValueError:
           continue

       if lineType == 'Dz':
           Dz.append(n)
       elif lineType == 'Nz':
           Nz.append(n)


print("("+gen(Nz)+")/("+gen(Dz)+")")
