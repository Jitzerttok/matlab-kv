classdef AnalogFilter 
   properties
      Ns, Ds
   end
   
   methods
       function obj = AnalogFilter(Ns, Ds)
           obj.Ns=Ns;
           obj.Ds=Ds;
       end
       
       function disp( this )
           fprintf('Analog Filter\n');
           format long g
           fprintf('Ns: \n');
           disp(this.Ns');
           fprintf('\nDs: \n');
           disp(this.Ds');
           [z,p,~]=tf2zp(this.Ns, this.Ds);
           fprintf('\nPoles: \n');
           disp(p)
           fprintf('\nZeros: \n');
           disp(z)
       end
        
       function [T, w] = freqs( this, points )
            [T, w]=freqs(this.Ns, this.Ds, points);
       end
       
       function plotAttenuationNoFig( this, points, contrain)
           [T, w]=this.freqs(points);
           plot(w/(2*pi), -20*log10(abs(T))); grid;
           xlabel('Frequency[Hz]');
           ylabel('Attenuation[db]');
           title('Analog Filter Attenuation');
           axis(contrain)
       end
       
       function plotPhaseNoFig( this, points, contrain)
           [T, w]=this.freqs(points);
           plot(w/(2*pi), unwrap(angle(T))*180/pi); grid;
           xlabel('Frequency[Hz]');
           ylabel('Phase[deg]');
           title('Analog Filter PFX');
           axis(contrain)
       end
       
       function plotImpuseResponseNoFig( this, contrain)
           [imp, ~, t] = impulse(this.Ns, this.Ds);
           plot(t, imp); grid;
           xlabel('Time');
           ylabel('Magnitude');
           title('Analog Filter Impuse Response');
           axis(contrain)
       end
       
       function pzmap(this)
              pzmap(this.Ns, this.Ds);
              title('Analog Filter Pole Zero Plot');
       end
   end
   
   
end