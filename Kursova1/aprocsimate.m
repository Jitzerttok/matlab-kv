function [AF, DF] = aprocsimate( Ws, Wp, Rp, Rs, Fs )
    [N,Wn]=ellipord(Wp,Ws,Rp,Rs,'s');
    disp(N)
    [Ns, Ds]=ellip(N, Rp, Rs, Wn, 'stop', 's');
    [Nz, Dz]=bilinear(Ns, Ds, Fs); 
    AF = AnalogFilter(Ns,Ds);
    DF = DigitalFilter(Nz,Dz,Fs);
end

