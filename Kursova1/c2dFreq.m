function [ w ] = c2dFreq( W, Fs )
    T=1/Fs;
    w=2/T*atan(T*W/2);
end

