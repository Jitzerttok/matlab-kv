classdef DigitalFilter
   properties
      Nz, Dz, Fs
   end
   
   methods
       function obj = DigitalFilter(Nz, Dz, Fs)
           obj.Nz=Nz;
           obj.Dz=Dz;
           obj.Fs=Fs;
       end
       
       function disp( this )
           format long g
           fprintf('Digital Filter\n');
           fprintf('Nz: \n');
           disp(this.Nz');
           fprintf('\nDz: \n');
           disp(this.Dz');
           [z,p,~]=tf2zp(this.Nz, this.Dz);
           fprintf('\nPoles: \n');
           disp(p)
           fprintf('\nZeros: \n');
           disp(z)
           
       end
        
       function [T, w] = freqz( this, points )
            [T, w]=freqz(this.Nz, this.Dz, points);
       end
       
       function plotAttenuation( this, fig, points, contrain)
           figure(fig);
           [T, w]=this.freqz(points);
           plot(w*this.Fs/(2*pi), -20*log10(abs(T))); grid;
           axis(contrain)
       end
       
       function plotAttenuationNoFig( this, points, contrain)
           [T, w]=this.freqz(points);
           plot(w*this.Fs/(2*pi), -20*log10(abs(T))); grid;
           xlabel('Frequency[Hz]');
           ylabel('Attenuation[db]');
           title('Digital Filter Attenuation');
           axis(contrain)
       end
       
       function plotPhaseNoFig( this, points, contrain)
           [T, w]=this.freqz(points);
           plot(w*this.Fs/(2*pi), unwrap(angle(T))*180/pi); grid;
           xlabel('Frequency[Hz]');
           ylabel('Phase[deg]');
           title('Digital Filter PFX');
           axis(contrain)
       end
       
       function pzmap(this)
              zplane(this.Nz, this.Dz);
              title('Digital Filter Pole Zero Plot');
       end
       
       function plotImpuseResponseNoFig( this, contrain)
           impz(this.Nz, this.Dz); grid;
           xlabel('n');
           ylabel('Magnitude');
           title('Digital Filter Impuse Response');
           axis(contrain)
       end
       
       function plotoutput(this, x, fig, msg, K)
              if nargin < 5
                  K=1024;
              end
              if nargin < 4
                  msg='';
              end
            n=(0:99)/this.Fs;
            signal=x(n);
            figure(fig);
            subplot(221);
            plot(n*this.Fs, signal); grid;
            xlabel('n(samples)');
            ylabel('Amplitude');
            title(strcat('Input signal', {' '}, msg));
            Px=fft(signal, K);
            px=abs(Px(1:K/2));
            subplot(222);
            [~, w]=this.freqz(K/2);
            plot(w*this.Fs/(2*pi), px); grid;
            xlabel('Freqency[Hz]');
            ylabel('Amplitude');
            title('Input signal spectrum');
            y=filter(this.Nz, this.Dz, signal);
            subplot(223);
            plot(n*this.Fs, y); grid;
            xlabel('n(samples)');
            ylabel('Amplitude');
            title('Output signal');
            Py=fft(y, K);
            py=abs(Py(1:K/2));
            subplot(224);
            plot(w*this.Fs/(2*pi), py); grid;
            xlabel('Freqency[Hz]');
            ylabel('Amplitude');
            title('Output signal spectrum');
        end
   end
   
   
end

