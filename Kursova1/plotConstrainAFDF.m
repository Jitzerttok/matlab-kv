function plotConstrainAFDF(fig1, fig2, Ws, Wp, Rp, Rs, Fs )
    constrain=[0 Fs/2 0 50];
    color=[ 0.888 0.888 0.888];
    figure(fig1);
    rectangle('Position', [0 Rp Wp(1)/(2*pi) 100000], 'EdgeColor', color, 'FaceColor', color);
    rectangle('Position', [Wp(2)/(2*pi) Rp 100000 100000], 'EdgeColor', color, 'FaceColor', color);
    rectangle('Position', [Ws(1)/(2*pi) 0 (Ws(2)-Ws(1))/(2*pi) Rs], 'EdgeColor', color, 'FaceColor', color);
    axis(constrain);
    xlabel('Frequency[Hz]');
    ylabel('Attenuation[db]');
    title('Analog Filter Constrains');
    figure(fig2);
    rectangle('Position', [0 Rp c2dFreq(Wp(1), Fs)/(2*pi) 100000], 'EdgeColor', color, 'FaceColor', color);
    rectangle('Position', [c2dFreq(Wp(2), Fs)/(2*pi) Rp 100000 100000], 'EdgeColor', color, 'FaceColor', color);
    rectangle('Position', [c2dFreq(Ws(1), Fs)/(2*pi) 0 c2dFreq(Ws(2)-Ws(1), Fs)/(2*pi) Rs], 'EdgeColor', color, 'FaceColor', color);
    axis(constrain);
    xlabel('Frequency[Hz]');
    ylabel('Attenuation[db]');
    title('Digital Filter Constrains');
end

