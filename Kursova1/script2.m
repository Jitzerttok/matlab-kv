close all
clear all
clc

Ws=[921 961]*(2*pi);
Wp=[865 1030]*(2*pi);
Rs=25;     
Rp=0.1;
Fs=4660;
plotConstrainAFDF(11, 12, Ws, Wp, Rp, Rs, Fs );
[AF, DF] = aprocsimate( Ws, Wp, Rp, Rs, Fs );
plotAttenuationAFDF( AF, DF, 1, 13, Ws, Wp, Rp, Rs, Fs );

figure(2);
AF.plotPhaseNoFig(5000,[0 2000 -360 360]);
figure(3);
DF.plotPhaseNoFig(5000,[0 2000 -360 360]);

figure(4);
AF.pzmap();
figure(5);
DF.pzmap();

figure(6);
AF.plotImpuseResponseNoFig([]);
figure(7);
DF.plotImpuseResponseNoFig([0 200 -1.2 1.2]);

f=840;
signal=@(n)sin((2*pi*f).*n);
DF.plotoutput(signal, 8, sprintf('f=%dHz',f));
f=700;
signal2=@(n)sin((2*pi*f).*n);
DF.plotoutput(signal2, 9, sprintf('f=%dHz',f));

disp(AF)
disp(DF)