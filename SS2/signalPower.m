function [ p ] = signalPower(signal, period)
        syms t
    p = double(int(signal^2, t, 0, period)/period);

end

