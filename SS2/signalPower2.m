function [ p ] = signalPower2( signal, period )
    syms t
    p = double(int(signal^2, t, 0, period));
    
end

