syms t tao
A=1;
alfa=1;

signal = A*exp(-alfa*t)*heaviside(t);

figure(1);
ezplot(signal);
title('������ ������', 'FontName', 'ZepfChancery');
grid;

signalCopy = subs(signal, t, t-tao);
akf = int(signal*signalCopy, t, 0, 10);

figure(2);
ezplot(akf);
title('���� ������������ �������', 'FontName', 'ZepfChancery');
grid;