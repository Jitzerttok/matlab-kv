close all
clear all

syms t;

nRows = 25;
A = 5;
T = 5*10^-3;

signal = -A/T*t+A;

figure(1);
ezplot(signal, [0 T]);
xlabel('Time[s]');
ylabel('Magnitude[V]');
title('Signal');
grid;
[spectrum, freaqencies] = fourierCalculator( signal, T, nRows );

for i=1:nRows
    fprintf('Harmonic #%d Magnitude[V]: %f Phase[deg]: %f\n',i,abs(spectrum(i+1)),angle(spectrum(i+1))*180/pi); %�������� ���������� � ������ �� ����������� 
end

figure(2);
subplot(211)
stem(freaqencies/(2*pi), abs(spectrum)); %����� ���������� �������
xlabel('Freaqency[Hz]');
ylabel('Magnitude[V]');
title('AFSD');
grid;
subplot(212)
stem(freaqencies/(2*pi), angle(spectrum)*180/pi); %��������� ��� �������
xlabel('Freaqency[Hz]');
ylabel('Phase[deg]');
title('PFSD');
grid;

avgPower = signalPower( signal, T ); %��������� ���������� �� �������
fprintf('Signal average power: %f W\n', avgPower);
[sctPowerAvg, sctPower] = signalPowerSpectrum(spectrum, 3);% ��������� ������ ������� �� �����������
for i=1:3
    fprintf('Harmonic #%d Power: %f W\n',i, sctPower(i));
end
persent = sctPowerAvg/avgPower; %����������� ����� ����� �� � �������
fprintf('Average from signal: %f%%\n', persent*100);


