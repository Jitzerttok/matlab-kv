clear all;
close all;

% ���������� �� ���������� ����������
period = 2;
broj_povtorenia = 2;
broj_ot4eti_za_period = 50;
num_furie_coef = broj_ot4eti_za_period;
delta_epsilon = 10^-6;
zero_fixer = 1; % 0 - off; 1, 2, ... - on
pi_fixer = 1; % 0 - off; 1, 2, ... - on
time = 0:period/broj_ot4eti_za_period:period*broj_povtorenia - period/broj_ot4eti_za_period;
s1 = [1 1 1 0 0 0];

% ���������� �� �������� ����������� �������
koef_zapalvane = 0.25;
ampl_up = 1;
ampl_down = 0;
signal_lenght = broj_povtorenia*broj_ot4eti_za_period;
signal = zeros(1,signal_lenght);
cur_period_num = 1;
for i=0:signal_lenght-1;
    signal(i+1)=s1(rem(i, length(s1))+1);
end

% �������� �� ���� ������ �� ����������
signal_1_period = zeros(1,broj_ot4eti_za_period);
for k = 1:broj_ot4eti_za_period,
    signal_1_period(k) = signal(k);
end

% ����� ��������� ������������� �� �����
[spectral, frequency] = digital_fourie_calculator(signal_1_period, period, num_furie_coef);

% ���������� �� ����������� � ������� ������� �� �������
A4SH = abs(spectral);
P4SH = angle(spectral);

% ���������� �� ������ �� �������� ��������
if (zero_fixer ~= 0)
    for k = 1:num_furie_coef,
        if (A4SH(k)/max(A4SH) < delta_epsilon)
            P4SH(k) = 0;
        end
    end
end

% ������������ �� +180 � -180 ����������
if (pi_fixer ~= 0)
    for k = 1:num_furie_coef,
        if (abs(abs(P4SH(k)) - pi) < delta_epsilon)
            if (k <= num_furie_coef/2)
                P4SH(k) = pi;
            else
                P4SH(k) = - pi;
            end
        end
    end
end
% ����������� �� ���������� � ����������� ������ � �������� ��
figure (1);
subplot(3, 1, 1);
stem(time, signal);
hold on;
%stem(time, signal_recovery, 'r');
axis([min(time) max(time) min(signal) max(signal)]);
ylabel('magnitude [V]');
xlabel('time [sec]');
title('signals');
legend('original signal', 'reconstructed signal');
subplot(3, 1, 2);
stem(frequency/(2*pi),A4SH);
axis([0 max(frequency/(2*pi)) 0 1.1 * max(A4SH)]);
xlabel('frequency [Hz]');
ylabel('magnitude [V]');
title('AFSD');
subplot(3, 1, 3);
stem(frequency/(2*pi),P4SH);
xlabel('frequency [Hz]');
ylabel('phase [rad]');
title('PFSD');
figure (2);
subplot(211);
stem(time, signal);
subplot(212);
stem(time, signal_recovery, 'r');
% ������� ����� ������������� �� �����
signal_1_period_recovery = ifft(broj_ot4eti_za_period * spectral);
period_recovery = 2*pi/frequency(2);

% ��������� �� �������� �� �������������� �������
signal_recovery = zeros(1,broj_povtorenia*broj_ot4eti_za_period);
for k = 0:broj_povtorenia-1
    for kk = 0:broj_ot4eti_za_period-1,
        signal_recovery((k+1)*broj_ot4eti_za_period + kk) = signal_1_period_recovery(kk+1);
    end
end










