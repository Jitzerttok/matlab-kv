close all
clear all

syms t k
A = 1;
T = 1;
N = 5;

s = A*heaviside(T/4-t)-A*heaviside((t-3*T/4)*(T/4-t))+A*heaviside(t-3*T/4);

fig1 = figure(1);
ezplot(s,[0, T]);
title('������');
xlabel('��������� [V]');
ylabel('����� [s]');
grid;

S = 2*int(s * exp(-1i * 2*pi/T *   k   * t), t, 0, T)/T;
S = 2*int(s * exp(-1i * 2*pi/T * (k-1) * t), t, 0, T)/T;
