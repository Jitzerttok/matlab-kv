Fs=4300;
Wp=[620 1200];
Ws=[920 1080];
Rs=30;
Rp=0.5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ������ �� ��������
figure(1);
hold on;
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
title('Gabarit');
xlabel('Freqency[Hz]');
ylabel('Attenuation[dB]'); %���������
axis([0 Fs/2 0 Rs+20]);

% ������������ �� ������ ��
[N, Wn]=cheb2ord(Wp/(Fs/2), Ws/(Fs/2), Rp, Rs);
[Nz, Dz]=cheby2(N,Rs, Wn,'stop');

% ���������� �� ����������� �� ��
fprintf('���: %d\n', N*2); 
fprintf('Nz:\n');
disp(Nz');
fprintf('Dz:\n');
disp(Dz');
[z,p,~]=tf2zp(Nz, Dz);
fprintf('������:\n');
disp(p);
fprintf('����:\n');
disp(z);

% ����������� �� ��������� � ���
[H, w]=freqz(Nz, Dz, 6969);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ������ �� ��������� + ��������
figure(2);
hold on;
plot(w*Fs/(2*pi), -20*log10(abs(H)));
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
title('Zatihvane na RF i Gabarit');
xlabel('Freqency[Hz]');
ylabel('Attenuation[dB]'); %���������
axis([0 Fs/2 0 50]);
% ��� ������� �� ���������� ������� �� ��
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ������ �� �������-�������� ��������
figure(3);
zplane(Nz,Dz);

% ������ �� ��� ��������
figure(4);
plot(w*Fs/(2*pi), unwrap(angle(H))*180/pi);
title('F4X'); grid;
xlabel('Freqency[Hz]');
ylabel('Phase[deg]'); %����
xlim([0 Fs/2]);

% ������ �� ��
figure(5);
impz(Nz, Dz); grid;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[b0, B, A]=dir2cas(Nz, Dz);
fprintf('����������� �� �������� ����������:\n');
fprintf('b0:\n');
disp(b0);
fprintf('B:\n');
disp(B);
fprintf('A:\n');
disp(A);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n=(0:99)/Fs;
K=1024;
%Filturut ne propuska
f=970;
signal=sin((2*pi*f).*n);
figure(6);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Magnitude');
xlabel('O4eti');
title('Input Signal f=970Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[~, w]=freqz(Nz,Dz, K/2);
plot(w*Fs/(2*pi), px); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Input Signal Spectrum');
y=filter(Nz, Dz, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylim([-1 1]);
ylabel('Magnitude');
xlabel('O4eti');
title('Output Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
ylim([0 60]);
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Output Signal Spectrum');

%Filturut propuska
f=300;
signal=sin((2*pi*f).*n);
figure(7);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Magnitude');
xlabel('O4eti');
title('Input Signal f=300Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[~, w]=freqz(Nz,Dz, K/2);
plot(w*Fs/(2*pi), px); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Input Signal Spectrum');
y=filter(Nz, Dz, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Magnitude');
xlabel('O4eti');
title('Output Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Output Signal Spectrum');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ����� 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[N, Wn]=cheb2ord(ad(Wp, Fs)*(2*pi), ad(Ws, Fs)*(2*pi), Rp, Rs,'s');
[Ns, Ds]=cheby2(N,Rs, Wn,'stop','s');
format long g
fprintf('Ns:\n');
disp(Ns');
fprintf('Ds:\n');
disp(Ds');
[T, ws]=freqs(Ns, Ds, 6969);
[H, wz]=freqz(Nz, Dz, 6969);
figure(8);
subplot(211);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
title('Zatihvane na cifrov RF i Gabarit');
xlabel('Freqency[Hz]');
ylabel('Attenuation[dB]');
axis([0 Fs/2 0 50]);
subplot(212);
hold on;
plot(ws/(2*pi), -20*log10(abs(T)));
plot([0 ad(Wp(1), Fs)], [Rp Rp],'r');
plot([ad(Wp(1), Fs) ad(Wp(1), Fs)], [Rp Rp+2],'r');
plot([ad(Wp(2), Fs) 10000], [Rp Rp],'r');
plot([ad(Wp(2), Fs) ad(Wp(2), Fs)], [Rp Rp+2],'r');
plot([ad(Ws(1), Fs) ad(Ws(1), Fs)], [0 Rs],'r');
plot([ad(Ws(1), Fs) ad(Ws(2), Fs)], [Rs Rs],'r');
plot([ad(Ws(2), Fs) ad(Ws(2), Fs)], [0 Rs],'r');
title('Zatihvane na cifrov RF i Gabarit');
xlabel('Freqency[Hz]');
ylabel('Attenuation[dB]');
axis([0 Fs/2 0 50]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
