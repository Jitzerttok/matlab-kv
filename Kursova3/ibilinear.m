function [ Ns, Ds ] = ibilinear( Nz, Dz, Fs )
    [Ns,Ds] = tfdata(d2c(tf(Nz, Dz,1/Fs),'tustin'));
    Ns = cell2mat(Ns);
    Ds = cell2mat(Ds);
end

