A=5;
T=2;
n=50;
syms t;

% sawtooth ������
signal = (2*A/T)*t*(heaviside(t)-heaviside(t-T/2));

[ spectrum, frequency ] = FT( signal, T, n );
[ recoverdSignal, ~ ] = IFT( spectrum, frequency );

figure(1);
stem(frequency/(2*pi), abs(spectrum));
title('A4S');
xlabel('Frequency[Hz}');
ylabel('Amplitude{V]');

figure(2);
stem(frequency/(2*pi), angle(spectrum)*180/pi);
title('F4S');
ylabel('Phase[deg}');
xlabel('Frequency[Hz}');

figure(3);
ezplot(signal, [0 T]);
title('Original Signal');
xlabel('Time[s}');
ylabel('Amplitude{V]');

figure(4);
ezplot(recoverdSignal, [0 T]);
title('Recovered Signal');
xlabel('Time[s}');
ylabel('Amplitude{V]');
