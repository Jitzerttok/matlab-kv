function [ signal, T ] = IFT( spectrum, frequency )
    syms t;
    [~,n]=size(frequency);
    T=1/(frequency(2)/(2*pi));
    signal=0;
    for k=1:n
        signal = signal + abs(spectrum(k)) * cos(frequency(k)*t + angle(spectrum(k)));
    end
end

