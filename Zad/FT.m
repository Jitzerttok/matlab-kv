function [ spectrum, frequency ] = FT( signal, T, n )
    syms t;
    spectrum=zeros(1, n+1);
    frequency=zeros(1, n+1);
    for k=1:n+1
        frequency(k)=2*pi*(k-1)/T;
        spectrum(k)=int(signal*exp(-1i*frequency(k)*t), t, 0, T);
    end
    spectrum(1)=spectrum(1)/2;
end

