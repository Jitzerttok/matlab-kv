close all;
clear all;
clc;

f1=10.0e9;
f2=12.0e9;
G=18; %dB
c=physconst('LightSpeed');

niA=0.7;
n=1.5; tgb=0.3; %Полиетилен
a=22.68e-3; b=10.16e-3; %WG=16
d=0.5; %5C-2V
k=1.45;
C1=0.15;
C2=1-C1;


f0=(f1+f2)/2;
lambda0 = c/f0; lambda1 = c/f1; lambda2 = c/f2; 
fprintf('f0=%.2fGHz lambda0=%.2fmm lambda1=%.2fmm lambda2=%.2fmm\n', f0*10^-9, lambda0*10^3, lambda1*10^3, lambda2*10^3);

lB=0.15*lambda0;

S=(db2mag(G)*lambda0^2)/(4*pi*niA);
fprintf('S=%.2fmm\n', S*10^6);

LE=sqrt(S/k);
LH=LE*k;
fprintf('LE=%.2fmm LH=%.2fmm\n', LE*10^3,LH*10^3);

syms y 
f=LH;
x(y) = -f/(n+1) + sqrt((f/(n+1))^2 + y^2/(4*(n^2-1)));
t=eval(subs(x,y,LH));
fprintf('t=%.2fmm\n', t*10^3);
fy = linspace(-LE/2, LE/2, 100);
fx=eval(subs(x,y,fy));
plot(fx,fy);
hold on;
plot([t t], [fy(1) fy(length(fy))]);
xlabel('x'); ylabel('y');
%plot([fx(1) t], [fy(length(fy)) fy(length(fy))]);
table({'y*10^3', 'x*10^3', '(t(y)=t-x)*10^3'}, [fy'.*10^3, fx'.*10^3, (t-fx)'.*10^3]);

fi=linspace(-90,90,100);
Fh = FH(C1,C2, fi, LH, lambda0);
figure(2);
polar(deg2rad(fi), Fh);
figure(3);
plot(fi, mag2db(Fh));
xlabel('tetha (deg)');
ylabel('F(tetha) dB');
table({'Fi', 'F(tetha)'}, [fi', Fh']);


tetha=linspace(-90,90,100);
Fe = FE(tetha, LE, lambda0);
figure(4);
polar(deg2rad(tetha), Fe);
figure(5);
plot(tetha, mag2db(Fe));
xlabel('fi (deg)');
ylabel('F(fi) dB');
table({'fi', 'F(fi)'}, [tetha', Fe']);

ni=exp(-(2*pi/lambda0)*t*n*tgb);
D=G/ni;
fprintf('ni=%.2f D=%.2fdB(%.2fmag)\n', ni, mag2db(D), D);

KBV=1/n;
if KBV<0.9
    fprintf('KBV=%.2f < 0.9\n', KBV);
else 
    fprintf('KBV=%.2f >= 0.9\n', KBV);
end

nd=sqrt(n);
dd=lambda0/(4*nd);
fprintf('nd=%.2f dd=%.2fmm\n', nd, dd*10^3);

da1=lambda2/(16*(n-1));
tri0=atan(LE/(2*(f+t)));
da2=lambda2/(4*(1-cos(tri0)));
da3=lambda2/(2*LH)*f*(f+n*t)/(2*n*t);
fprintf('da1=%.2fmm da2=%.2fmm da3=%.2fmm\n', da1*10^3, da2*10^3, da3*10^3);

lambdaB0=lambda0/sqrt(1-(lambda0/(2*a))^2);
fprintf('lambdaB0=%.2fmm\n', lambdaB0*10^3);

l=lambda0/4;
fprintf('l=%.2fmm\n', l*10^3);
fprintf('l/d=%.4f\n', l/d);

h0=lambda0/(2*pi)*tan(pi*lB/lambda0);
h1=lambda1/(2*pi)*tan(pi*lB/lambda1);
h2=lambda2/(2*pi)*tan(pi*lB/lambda2);
fprintf('h0=%.2fmm h1=%.2fmm h2=%.2fmm\n', h0*10^3, h1*10^3, h2*10^3);
c1=0.25*lambdaB0;
fprintf('c1=%.2fmm\n',c1*10^3);

Rinfinity0 =((120*pi*h0^2)/(a*b))*sin(pi/2)^2/sqrt(1-(lambda0/(2*a))^2);
Rinfinity1 =(120*pi*h1^2)/(a*b)*sin(pi/2)^2/sqrt(1-(lambda1/(2*a))^2);
Rinfinity2 =(120*pi*h2^2)/(a*b)*sin(pi/2)^2/sqrt(1-(lambda2/(2*a))^2);

fprintf('Rinf0=%.2fm Rinf1=%.2fOm Rinf2=%.2fOm\n', Rinfinity0, Rinfinity1, Rinfinity2);

R0=2*Rinfinity0*sin(2*pi/lambdaB0*c1);
R1=2*Rinfinity1*sin(2*pi/lambdaB0*c1);
R2=2*Rinfinity2*sin(2*pi/lambdaB0*c1);

fprintf('R0=%.2fm R1=%.2fOm R2=%.2fOm\n', R0, R1, R2);