close all;
clear all;
clc;

L=2.5e-6;
bl=4e-2;
f=15e6;
Q=250;
I=30e-3;
T=65;
fprintf('Input prams\n');
fprintf('L=%.2fuH\n', L*10^6);
fprintf('f=%.2fMHz\n', f*10^-6);
fprintf('bl=%.2fpercent\n', bl*10^2);
fprintf('Q=%.2f\n', Q);
fprintf('I=%.2fmA\n', I*10^3);
fprintf('dT=%.2fC\n', T);

p=1.2;
K1=6;
K2=1.7;
K3=0;
K4=0.21;
r0=138;
GZ=2.762;
FZ=5.911;
K5=0.65;
%Kermaika
tgb=1e-2;
Er=7;

fprintf('\nOutput prams\n');
D=Q/(0.85*K1*p*sqrt(f));
fprintf('D=%.2fmm\n', D*10^3);
b=p*D;
fprintf('b=%.2fmm\n', b*10^3);
N=ceil(sqrt((L*10^7)/(K1*D)));
fprintf('N=%.0f\n', N);
tau=b/(N-1);
d=tau/K2;
fprintf('d=%.2fmm\n', d*10^3);
dmin=(2*I*10^-2*nthroot(f,4))/(pi*55*sqrt(T));
fprintf('dmin=%.2fmm\n', dmin*10^3);
Ld=L-2*pi*(K3+K4)*N*D*10^-7;
fprintf('Ld=%.2fuH\n', Ld*10^6);
bld=(L-Ld)/L;
fprintf('bl=%.2f maximum %.2f\n', bld*10^2, bl*10^2);
Nprim=ceil(N*sqrt(L/Ld));
fprintf('Nprim=%.0f\n', Nprim);
z=10.6*d*sqrt(f);
fprintf('z=%.2f\n', z);
R0=N*pi*D*r0*10^-3;
Rf=R0*(1+FZ+((K5*N*d)/D)^2*GZ);
fprintf('Rf=%.6f\n', Rf);
CD=(50*D*10^-12)/(1+1/(0.08*Er));
RD=(2*pi*f)^3*L^2*CD*tgb;
fprintf('RD=%.6f\n', RD);
Q0=(2*pi*f*L)/(Rf+RD);
fprintf('Q0=%.2f must be above Q=%.2f\n', Q0, Q);


