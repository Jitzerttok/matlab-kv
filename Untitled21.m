close all
clear all
clc
format long g

K=1024;  
Fs=10550;
Fn=Fs/2;       
Rp=2;           
Rs=32;          
Wp=[300 3400];       
Ws=[60 4000]; 

figure(1);
hold on;
plot([0 Ws(1)],[Rs Rs],'b');  
plot([Ws(1) Ws(1)],[0 Rs],'b');  
plot([Wp(1) Wp(2)],[Rp Rp],'b');  
plot([Ws(2) Ws(2)],[0 Rs],'b');
plot([Ws(2) Ws(2)+600],[Rs Rs],'b'); 
axis([0 Ws(2)+600 0 Rs+3]);
title('���������� �� ��������� �������� ��');                
xlabel('������� � Hz');                                                  
ylabel('��������� � dB');   

%������������� �� ����������� ������� � �������      
Ws_cifrov=[(Fs*tan(pi*Ws(1)/Fs))/pi (Fs*atan(pi*Ws(2)/Fs))/pi];  
Wp_cifrov=[(Fs*tan(pi*Wp(1)/Fs))/pi (Fs*atan(pi*Wp(2)/Fs))/pi];

figure(10);
hold on;
plot([0 Ws_cifrov(1)],[Rs Rs],'b');  
plot([Ws_cifrov(1) Ws_cifrov(1)],[0 Rs],'b');  
plot([Wp_cifrov(1) Wp_cifrov(2)],[Rp Rp],'b');  
plot([Ws_cifrov(2) Ws_cifrov(2)],[0 Rs],'b');
plot([Ws_cifrov(2) Ws_cifrov(2)+100000],[Rs Rs],'b'); 
axis([0 Ws(2)+600 0 Rs+3]);
title('���������� �� ��������� ������ ��');                
xlabel('������� � Hz');                                                  
ylabel('��������� � dB'); 

[N,Wn]=ellipord(Wp*(2*pi),Ws*(2*pi),Rp,Rs,'s');      
[Ns,Ds]=ellip(N,Rp,Rs,Wn,'s');  
printsys(Ns,Ds,'s');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[Nz,Dz]=bilinear(Ns,Ds,Fs); 
filt(Nz,Dz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(2);
subplot(2,1,1);
[T,w]=freqs(Ns,Ds,2000);
m=abs(T);                   
m1=-20*log10(m);
 plot(w/(2*pi),m1); hold on
plot([0 Ws(1)], [Rs Rs]);  %gabaritite po x
plot([Ws(1) Ws(1)], [Rs 0], 'r'); 
plot([Wp(1) Wp(2)], [Rp Rp], 'r');  
 
plot([Ws(2) Ws(2)], [0 Rs], 'r');
plot([Ws(2) 10000], [Rs Rs], 'r');
 

xlabel('Frequency, Hz');
ylabel('Attenuation, dB');
title('Analog Filter');
axis([0 Fs/2 0 Rs+28]) 


subplot(2,1,2);
[T1,w1]=freqz(Nz,Dz,K/2);
m2=-20*log10(abs(T1));
plot(w1*Fs/(2*pi),m2);hold on
 
plot([0 Ws_cifrov(1)],[Rs Rs],'r');  
plot([Ws_cifrov(1) Ws_cifrov(1)],[0 Rs],'r');  
plot([Wp_cifrov(1) Wp_cifrov(2)],[Rp Rp],'r');  
plot([Ws_cifrov(2) Ws_cifrov(2)],[0 Rs],'r');
plot([Ws_cifrov(2) Ws_cifrov(2)+100000],[Rs Rs],'r'); 
axis([0 Fs/2 0 Rs+28]);

xlabel('Frequency, Hz');
ylabel('Attenuation, dB');
title('Digital Filter');


 

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
figure(3);
zplane(Nz,Dz);        %--��������� ���--
z=roots(Nz);          
p=roots(Dz);            
title('��� �� �� ')          
disp(z);
disp(p);

figure(4);
pzmap(Ns,Ds);        %--��������� ���--
z1=roots(Ns);          
p1=roots(Ds);            
title('��� �� ��')         
disp(z1);
disp(p1);                                
 

 
fi=angle(T);
fi1=unwrap(fi);
          
figure(5);
subplot(2,1,1);
plot(w/(2*pi),fi1*180/pi); 
xlabel('Frequency(Hz)');grid;
ylabel('FHX(Deg)');grid;
title('Fazova-Chestotna Harakteristika Analog');grid;     

subplot(2,1,2);
impulse(Ns,Ds);
xlabel('Time(sec)');
ylabel('Amplitude(volts)');
title('Impulsna Harakteristika Analog');

 %Fazova-chestotna i Impulsna harakteristika Digit%
 
 
fi2=angle(T);
fi3=unwrap(fi2);
 
figure(6);
subplot(2,1,1);
plot(w*Fs/(2*pi),fi3*180/pi);
xlabel('Frequency(Hz)');
ylabel('FCHX(Deg)');grid;
title('Fazova-Chestotna Harakteristika Digit');grid;

subplot(2,1,2);
impz(Nz,Dz);
                               % �� �� ������
xlabel('Otcheti vreme');
ylabel('Amplitude(volts)');
title('Impulsna Harakteristika Digit');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f1=45;
n=(0:99)/Fs;
x1 = sin(2*pi*f1*n);
figure(7);
subplot(4,1,1);plot(n,x1);grid;
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden vuv vremeto signal f=45Hz');
 
y1=filter(Nz,Dz,x1);
subplot(4,1,2);plot(n,y1);grid;
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden vuv vremeto signal');
 
Px1=fft(x1,K);
px1=abs(Px1(1:K/2));
subplot(4,1,3);plot(w1*Fs/(2*pi),px1); grid;
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na vhoden signal');
 
Yx1=fft(y1,K);
yx1=abs(Yx1(1:K/2));
subplot(4,1,4);plot(w1*Fs/(2*pi),yx1); grid;
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na izhoden signal');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f1=1500;
n=(0:99)/Fs;
x1 = sin(2*pi*f1*n);
figure(8);
subplot(4,1,1);plot(n,x1);grid;
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden vuv vremeto signal f=1500Hz');
 
y1=filter(Nz,Dz,x1);
subplot(4,1,2);plot(n,y1);grid;
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden vuv vremeto signal');
 
Px1=fft(x1,K);
px1=abs(Px1(1:K/2));
subplot(4,1,3);plot(w1*Fs/(2*pi),px1); grid;
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na vhoden signal');
 
Yx1=fft(y1,K);
yx1=abs(Yx1(1:K/2));
subplot(4,1,4);plot(w1*Fs/(2*pi),yx1); grid;
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na izhoden signal');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
