clear all
close all
clc
format long g
Fs=5240;
Wp=[1490 1800];
Ws=[1601 1665];
Rs = 25;
Rp = 0.1;
K=2048;

color = [0.7250 0.7250 0.7250];
granici=[0 Fs/2 0 30];

%���������� �������
figure(1);
rectangle('Position', [0 Rp Wp(1) 10000], 'FaceColor', color); % [x y w h] ������� (0 Rp) (����� ��� ���� �� �������������), ������� Wp(1) � �������� 10000 (����� � �� ������ �� ������� ���� �� ��������� ���� �� ������� ���� �� � � ���� ������)
rectangle('Position', [Ws(1) 0 (Ws(2)-Ws(1)) Rs], 'FaceColor', color); % ������� 
rectangle('Position', [Wp(2) Rp 10000 10000], 'FaceColor', color); % ������ 
axis(granici);
title('Gabarit na cifrov RF');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%������������� �������
figure(2);
rectangle('Position', [0 Rp Wp(1) 10000], 'FaceColor', color); % �����
rectangle('Position', [Ws(1) 0 10000 Rs], 'FaceColor', color); % ������ 
axis(granici);
title('Gabarit na cifrov N4F');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%�������������� �������
figure(3);
rectangle('Position', [0 0 Ws(2) Rs], 'FaceColor', color); % �����
rectangle('Position', [Wp(2) Rp 10000 10000], 'FaceColor', color); % ������
axis(granici);
title('Gabarit na cifrov V4F');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%������������ �� ��������� 
%������������ �� ������ ���
[NN4, WnN4] = buttord(Wp(1)/(Fs/2), Ws(1)/(Fs/2), Rp, Rs);
[NzN4, DzN4] = butter(NN4,  WnN4);
[HN4, wN4] = freqz(NzN4, DzN4, K/2);

%������������ �� ������ ���
[NV4, WnV4] = buttord(Wp(2)/(Fs/2), Ws(2)/(Fs/2), Rp, Rs);
[NzV4, DzV4] = butter(NV4,  WnV4, 'high');
[HV4, wV4] = freqz(NzV4, DzV4, K/2);

fprintf('������������� ������: \n'); 
fprintf('���: %d\n', NN4*2); 
filt(NzN4, DzN4)
[z,p,~]=tf2zp(NzN4, DzN4);
fprintf('������:\n');
disp(p);
fprintf('����:\n');
disp(z);

fprintf('�������������� ������: \n'); 
fprintf('���: %d\n', NV4*2); 
filt(NzV4, DzV4)
[z,p,~]=tf2zp(NzV4, DzV4);
fprintf('������:\n');
disp(p);
fprintf('����:\n');
disp(z);

%������������o ���������
figure(4);
subplot(211);
plot(wN4*Fs/(2*pi), -20*log10(abs(HN4)));
rectangle('Position', [0 Rp Wp(1) 10000], 'FaceColor', color); % ����� 
rectangle('Position', [Ws(1) 0 (Ws(2)-Ws(1)) Rs], 'FaceColor', color); % ������� 
rectangle('Position', [Wp(2) Rp 10000 10000], 'FaceColor', color); % ������  
axis(granici);
title('Zatihvane na cifrov N4F');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

%�������������o ���������
subplot(212);
plot(wV4*Fs/(2*pi), -20*log10(abs(HV4)));
rectangle('Position', [0 Rp Wp(1) 10000], 'FaceColor', color); % ����� 
rectangle('Position', [Ws(1) 0 (Ws(2)-Ws(1)) Rs], 'FaceColor', color); % ������� 
rectangle('Position', [Wp(2) Rp 10000 10000], 'FaceColor', color); % ������ 
axis(granici);
title('Zatihvane na cifrov V4F');
ylabel('Zatihvane[dB]');
xlabel('Freqency[Hz]');

% ���
figure(5);
zplane(NzN4, DzN4);
title('PND na cifrov N4F');
figure(6);
zplane(NzV4, DzV4);
title('PND na cifrov V4F');

% ���
figure(7);
plot(wN4*Fs/(2*pi), unwrap(angle(HN4))*180/pi);
grid;
title('F4X na cifrov N4F');
ylabel('Faza[deg]');
xlabel('Freqency[Hz]');
xlim([0 Fs/2]);
figure(8);
plot(wV4*Fs/(2*pi), unwrap(angle(HV4))*180/pi); 
grid;
xlim([0 Fs/2]);
title('F4X na cifrov V4F');
ylabel('Faza[deg]');
xlabel('Freqency[Hz]');

% ��
figure(9);
impz(NzN4, DzN4); grid;
title('IX na cifrov N4F');
figure(10); 
impz(NzV4, DzV4); grid;
title('IX na cifrov V4F');

n=(0:99)/Fs;

%Filturut ne propuska
f=1640;
signal=sin((2*pi*f).*n);
figure(11);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Amplituda');
xlabel('O4eti');
title('Vhoden Signal 1640Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
plot(wN4*Fs/(2*pi), px); grid;
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Spectrur na Vhoden Signal');
y1=filter(NzN4, DzN4, signal);
y2=filter(NzV4, DzV4, signal);
y=y1+y2;
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Amplituda');
xlabel('O4eti');
title('Izhoden Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(wN4*Fs/(2*pi), py); grid;
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Spectrur na Izhoden Signal');

%Filturut propuska
f=400;
signal=sin((2*pi*f).*n);
figure(12);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Amplituda');
xlabel('O4eti');
title('Vhoden Signal 400Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
plot(wN4*Fs/(2*pi), px); grid;
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Spectrur na Vhoden Signal');
y1=filter(NzN4, DzN4, signal);
y2=filter(NzV4, DzV4, signal);
y=y1+y2; % ��� 31-32
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Amplituda');
xlabel('O4eti');
title('Izhoden Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(wN4*Fs/(2*pi), py); grid;
ylabel('Amplituda');
xlabel('Frequency[Hz]');
title('Spectrur na Izhoden Signal');