Wp=[60 3900];
Ws=[300 3400];
Rs=28;
Rp=2;
Fs=12660;

% To4ka 1
figure(1);
hold on;
axis([0 Fs/2 0 40]);
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');

title('Utejnen analogov rejektoren filtur');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');

Wp_digital=[AnalogovoCifrovoPreobrazuvane(Wp(1), Fs) AnalogovoCifrovoPreobrazuvane(Wp(2), Fs)];
Ws_digital=[AnalogovoCifrovoPreobrazuvane(Ws(1), Fs) AnalogovoCifrovoPreobrazuvane(Ws(2), Fs)];
figure(2);
hold on;
axis([0 Fs/2 0 40]);
plot([0 Wp_digital(1)], [Rp Rp],'r');
plot([Wp_digital(1) Wp_digital(1)], [Rp Rp+2],'r');
plot([Wp_digital(2) 10000], [Rp Rp],'r');
plot([Wp_digital(2) Wp_digital(2)], [Rp Rp+2],'r');
plot([Ws_digital(1) Ws_digital(1)], [0 Rs],'r');
plot([Ws_digital(1) Ws_digital(2)], [Rs Rs],'r');
plot([Ws_digital(2) Ws_digital(2)], [0 Rs],'r');
title('Utejnen cifrov rejektoren filtur');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');

[N, Wn]=cheb2ord(Wp*(2*pi), Ws*(2*pi), Rp, Rs, 's');
[Ns, Ds]=cheby2(N, Rs, Wn, 'stop', 's');
printsys(Ns, Ds);
fprintf('Ns:\n');
disp(Ns');
fprintf('Ds:\n');
disp(Ds');

% To4ka 2
[Nz, Dz]=bilinear(Ns, Ds, Fs);
filt(Nz, Dz, Fs)
fprintf('Nz:\n');
disp(Nz');
fprintf('Dz:\n');
disp(Dz');

% To4ka 3
[T, ws]=freqs(Ns, Ds, 5000);
[H, wz]=freqz(Nz, Dz, 5000);

figure(3);
subplot(211);
hold on;
plot(ws/(2*pi), -20*log10(abs(T)));
axis([0 Fs/2 0 40]);
plot([0 Wp(1)], [Rp Rp],'r');
plot([Wp(1) Wp(1)], [Rp Rp+2],'r');
plot([Wp(2) 10000], [Rp Rp],'r');
plot([Wp(2) Wp(2)], [Rp Rp+2],'r');
plot([Ws(1) Ws(1)], [0 Rs],'r');
plot([Ws(1) Ws(2)], [Rs Rs],'r');
plot([Ws(2) Ws(2)], [0 Rs],'r');
title('Zatihvane na analogov rejektoren filtur');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');
subplot(212);
hold on;
plot(wz*Fs/(2*pi), -20*log10(abs(H)));
axis([0 Fs/2 0 40]);
plot([0 Wp_digital(1)], [Rp Rp],'r');
plot([Wp_digital(1) Wp_digital(1)], [Rp Rp+2],'r');
plot([Wp_digital(2) 10000], [Rp Rp],'r');
plot([Wp_digital(2) Wp_digital(2)], [Rp Rp+2],'r');
plot([Ws_digital(1) Ws_digital(1)], [0 Rs],'r');
plot([Ws_digital(1) Ws_digital(2)], [Rs Rs],'r');
plot([Ws_digital(2) Ws_digital(2)], [0 Rs],'r');
title('Zatihvane na cifrov rejektoren filtur');
xlabel('4estota[Hz]');
ylabel('Zatihvane[dB]');

% To4ka 4
figure(4);
pzmap(Ns, Ds);
title('Polusno Nuleva Diagrama na Analoviqt filtur');
figure(5);
title('Polusno Nuleva Diagrama na Cifroviqt filtur');
zplane(Nz, Dz);

fprintf('Polusi i Nuli na Analoviqt filtur\n');
[z, p, ~]=tf2zpk(Ns, Ds);
fprintf('Polusi\n');
disp(p);
fprintf('Nuli\n');
disp(z);

fprintf('Polusi i Nuli na Cifroviqt filtur\n');
[z, p, ~]=tf2zpk(Nz, Dz);
fprintf('Polusi\n');
disp(p);
fprintf('Nuli\n');
disp(z);

figure(5);
plot(ws/(2*pi), unwrap(angle(T))*180/pi);
title('F4H na analogov rejektoren filtur');
xlabel('4estota[Hz]');
ylabel('Faza[deg]');
xlim([0 5000]);

figure(6);
plot(wz*Fs/(2*pi), unwrap(angle(H))*180/pi);
title('F4H na cifroviqt rejektoren filtur');
xlabel('4estota[Hz]');
ylabel('Faza[deg]');
xlim([0 5000]);

figure(7);
[imp,~,t]=impulse(Ns, Ds);
plot(t, imp);
title('IH na analogov rejektoren filtur');
xlabel('Vreme');
ylabel('Amplituda');

figure(8);
impz(Nz, Dz);
title('IH na analogov rejektoren filtur');

% To4ka 5
[C, B, A]=dir2par(Nz, Dz);
fprintf('C=\n');
disp(C);
fprintf('B=\n');
disp(B);
fprintf('A=\n');
disp(A);

% To4ka 6
f1=1500;
n=(0:99)/Fs;
x1 = sin(2*pi*f1*n);
figure(9);
subplot(2,2,1);plot(n,x1);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden vuv vremeto signal f=1500Hz');
 
y1=filter(Nz,Dz,x1);
subplot(2,2,2);plot(n,y1);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden vuv vremeto signal');
 
Px1=fft(x1,K);
px1=abs(Px1(1:K/2));
subplot(2,2,3);plot(w1*Fs/(2*pi),px1); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na vhoden signal');
 
Yx1=fft(y1,K);
yx1=abs(Yx1(1:K/2));
subplot(2,2,4);plot(w1*Fs/(2*pi),yx1); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na izhoden signal');

f1=4000;
n=(0:99)/Fs;
x1 = sin(2*pi*f1*n);
figure(10);
subplot(2,2,1);plot(n,x1);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Vhoden vuv vremeto signal f=4000Hz');
 
y1=filter(Nz,Dz,x1);
subplot(2,2,2);plot(n,y1);grid;
ylim([-1 1]);
xlabel('Otcheti na vremeto n');
ylabel('Amplituda v dB');
title('Izhoden vuv vremeto signal');
 
Px1=fft(x1,K);
px1=abs(Px1(1:K/2));
subplot(2,2,3);plot(w1*Fs/(2*pi),px1); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na vhoden signal');
 
Yx1=fft(y1,K);
yx1=abs(Yx1(1:K/2));
subplot(2,2,4);plot(w1*Fs/(2*pi),yx1); grid;
ylim([0 60]);
xlabel('Otcheti na vremeto v Hz');
ylabel('Amplituda v dB');
title('Amplituden spektur na izhoden signal');