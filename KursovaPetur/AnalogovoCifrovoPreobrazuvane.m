function [ w ] = AnalogovoCifrovoPreobrazuvane( W, Fs )
	T=1/Fs;
    W=W*(2*pi);
    w=2/T*atan(W*T/2);
    w=w/(2*pi);
end

