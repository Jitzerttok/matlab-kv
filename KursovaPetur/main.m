clc;
clear all;
close all;
format long g

Fs=2200;
Wp=[340 500]/(Fs/2);
Ws=[300 550]/(Fs/2);
Rp=3;
Rs=45;
K=1024;

[N,Wn]=cheb2ord(Wp(2),Ws(2),Rp,Rs);
[Nz,Dz]=cheby2(N,Rs, Wn);

[N1,Wn1]=cheb2ord(Wp(1),Ws(1),Rp,Rs);
[Nz1,Dz1]=cheby2(N1,Rs, Wn1,'high');

[H,w]=freqz(Nz,Dz,K/2);
[H1,w1]=freqz(Nz1,Dz1,K/2);

figure(1);
subplot(211);
plot(w*Fs/(2*pi), -20*log10(abs(H))); grid;
rectangle('Position', [0 0 Ws(1)*(Fs/2) Rs]);
rectangle('Position', [Wp(1)*(Fs/2) Rp (Wp(2)-Wp(1))*(Fs/2) 100000]);
rectangle('Position', [Ws(2)*(Fs/2) 0 1000000 Rs]);
axis([0 1200 0 250])

subplot(212);
plot(w1*Fs/(2*pi), -20*log10(abs(H1))); grid;
rectangle('Position', [0 0 Ws(1)*(Fs/2) Rs]);
rectangle('Position', [Wp(1)*(Fs/2) Rp (Wp(2)-Wp(1))*(Fs/2) 100000]);
rectangle('Position', [Ws(2)*(Fs/2) 0 1000000 Rs]);
axis([0 1200 0 250])
%TZU Kontolna 1 Vremediagrama na naprevenieto

fprintf('Low Pass Filter:\n');
[z,p,~]=tf2zp(Nz, Dz);
fprintf('Nz:\n');
disp(Nz');
fprintf('Dz:\n');
disp(Dz');
fprintf('Pole:\n');
disp(z);
fprintf('Zero:\n');
disp(p);
fprintf('\n');
fprintf('High Pass Filter:\n');
[z1,p1,~]=tf2zp(Nz1, Dz1);
fprintf('Nz:\n');
disp(Nz1');
fprintf('Dz:\n');
disp(Dz1');
fprintf('Pole:\n');
disp(z1);
fprintf('Zero:\n');
disp(p1);
fprintf('\n');

figure(2);
title('Low Pass Filter');
zplane(Nz, Dz);
figure(3);
title('High Pass Filter');
zplane(Nz1, Dz1);

figure(4);
title('Low Pass Filter');
plot(w*Fs/(2*pi), unwrap(angle(H))*180/pi); grid;
figure(5);
title('High Pass Filter');
plot(w*Fs/(2*pi), unwrap(angle(H1))*180/pi); grid;

figure(6);
title('Low Pass Filter');
[h,t] = impz(Nz,Dz);
stem(t,h); grid;
figure(7);
title('High Pass Filter');
[h1,t1] = impz(Nz1,Dz1);
stem(t1,h1); grid;

Nz2 = conv(Nz1, Nz);
Dz2 = conv(Dz1, Dz);

[C, B, A] = dir2par(Nz1, Dz1);
fprintf('C=\n'); disp(C);
fprintf('B=\n'); disp(B);
fprintf('A=\n'); disp(A);
n=(0:99)/Fs;

%Filturut propuska
f=490;
signal=sin((2*pi*f).*n);
figure(7);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Magnitude');
xlabel('O4eti');
title('Input Signal f=850Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[H2, w]=freqz(Nz2,Dz2, K/2);
plot(w*Fs/(2*pi), px); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Input Signal Spectrum');
y=filter(Nz2, Dz2, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Magnitude');
xlabel('O4eti');
title('Output Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
ylim([0 60]);
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Output Signal Spectrum');

%Filturut ne propuska
f=200;
signal=sin((2*pi*f).*n);
figure(8);
subplot(221);
plot(n*Fs, signal); grid;
ylabel('Magnitude');
xlabel('O4eti');
title('Input Signal f=200Hz');
Px=fft(signal, K);
px=abs(Px(1:K/2));
subplot(222);
[H2, w]=freqz(Nz2,Dz2, K/2);
plot(w*Fs/(2*pi), px); grid;
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Input Signal Spectrum');
y=filter(Nz2, Dz2, signal);
subplot(223);
plot(n*Fs, y); grid;
ylim([-1 1]);
ylabel('Magnitude');
xlabel('O4eti');
title('Output Signal');
Py=fft(y, K);
py=abs(Py(1:K/2));
subplot(224);
plot(w*Fs/(2*pi), py); grid;
ylim([0 60]);
ylabel('Magnitude');
xlabel('Frequency[Hz]');
title('Output Signal Spectrum');

[H_L, w_L]=freqz(Nz2,Dz2,5000);
figure(9);
plot(w_L*Fs/(2*pi), -20*log10(abs(H_L))); grid;
rectangle('Position', [0 0 Ws(1)*(Fs/2) Rs]);
rectangle('Position', [Wp(1)*(Fs/2) Rp (Wp(2)-Wp(1))*(Fs/2) 100000]);
rectangle('Position', [Ws(2)*(Fs/2) 0 1000000 Rs]);
title('Band Pass Filter Gabarit');
ylabel('Attenuation[dB]');
xlabel('Frequency[Hz]');
axis([0 1200 0 250]); grid;